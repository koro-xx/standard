/*
 gcc -Ofast -I/usr/local/include -L/usr/local/lib -lallegro -lallegro_main -lallegro_font -lallegro_image -lallegro_primitives cmdline.c

 * Version with palette and blending
 * TODO:
    - Use coordinate transform for mouse & display. Allow resizing. 
    - check coordinates (not matching portrait vs interior)
    - try different blendings
    - option to increase size for savefile
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include "macros.h"
#include "calc.h"
#include "graphics.h"
#include "mini_graphics.h"


/* Allegro stuff */
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>

#define MAX_PATH_LENGTH 256

ALLEGRO_EVENT_QUEUE *queue = NULL;
ALLEGRO_FONT *default_font = NULL;
ALLEGRO_BITMAP *aux_port_bmp = NULL;


#define KEY_PAL ALLEGRO_KEY_FULLSTOP
#define KEY_PALM ALLEGRO_KEY_COMMA
volatile sig_atomic_t save_and_exit = 0;
int colorize = 1;
int colorize_port = 1;
float iter_discard = 0.2;
int blend_colors = 0;
ALLEGRO_COLOR port_bg_color = (ALLEGRO_COLOR){0,0,0,255};
ALLEGRO_COLOR *palette = NULL;
int palsize = 255*255;
char *pal_file[512];
int pal_num = 0;
int pal_on = 0;
int draw_fast = 0;
int ctype_x[] = {1, 0, 1, 0};
int ctype_y[] = {1, 1, 1, 2};
int draw_shearless = 1;

float port_scale = 2;

static inline int pmod(int i, int n) {
    return (i % n + n) % n;
}

pixel get_col(dtable *t, int n)
{
    if(!t->ok[n] && itox(t, n) >= itoy(t, n))
        return colorize ? COLOR_RGBA((int)(t->my[n]*2*255), 0, 0, 255) : COLOR_BLACK;
    else
        return COLOR_WHITE;
}

ALLEGRO_COLOR color_blend_add(ALLEGRO_COLOR p1, ALLEGRO_COLOR p2, float t)
{
    
    if((t<0) || (t>1))
        t=t-floor(t);
    //return (ALLEGRO_COLOR) {sqrt((1-t)*p1.r*p1.r+t*p2.r*p2.r), sqrt((1-t)*p1.g*p1.g+t*p2.g*p2.g), sqrt((1-t)*p1.b*p1.b+t*p2.b*p2.b), 1};
    return (ALLEGRO_COLOR) {(1-t)*p1.r+t*p2.r, (1-t)*p1.g+t*p2.g, (1-t)*p1.b+t*p2.b, 1};
}


ALLEGRO_COLOR color_blend(ALLEGRO_COLOR p1, ALLEGRO_COLOR p2, float t)
{
    
    if((t<0) || (t>1))
        t=t-floor(t);

     //return (ALLEGRO_COLOR) {sqrt((1-t)*p1.r*p1.r+t*p2.r*p2.r), sqrt((1-t)*p1.g*p1.g+t*p2.g*p2.g), sqrt((1-t)*p1.b*p1.b+t*p2.b*p2.b), 1};
    return (ALLEGRO_COLOR) {(1-t)*p1.r+t*p2.r, (1-t)*p1.g+t*p2.g, (1-t)*p1.b+t*p2.b, 1};
}


#define MAX_PALSIZE (256*256)
#define MAX_LINE_SIZE 128

ALLEGRO_COLOR *load_palette_al(const char *filename, int palsize){
    char sline[MAX_LINE_SIZE];
    ALLEGRO_COLOR *pal_temp=NULL;
    int i=0;
    FILE *pal;
    int r, g, b;
    int n = palsize;
    ALLEGRO_COLOR *paleta = NULL;
    
    pal_temp = malloc((MAX_PALSIZE+1)*sizeof(*pal_temp));
    pal = fopen(filename,"r");
    if(!pal) goto Error;
    
    paleta = malloc(palsize*sizeof(*paleta));
    while( (i<MAX_PALSIZE) && (fgets(sline, MAX_LINE_SIZE-1, pal)) )
    {
        if(sscanf(sline,"%d %d %d",&r,&g,&b)==3)
            pal_temp[i].a = 1; pal_temp[i].b=b/255.0; pal_temp[i].g = g/255.0; pal_temp[i].r=r/255.0; i++;
    }
    fclose(pal);
    palsize=i;
    if(palsize < 2) goto Error;
    
    float t=((float)palsize)/n;
    for(i=0;i<n;i++)
    {
        if(t*i +1 <=palsize)
            paleta[i] = color_blend_add(pal_temp[(int)(t*i)], pal_temp[(int)(t*i) + 1], (t*i - floor(t*i)));
        else
            paleta[i]=pal_temp[(int) t*i];
    }

    return paleta;
    
Error:
    if(paleta) free(paleta);
    return NULL;
    
}


/* Assumes bmp has size t->xsize, t->ysize, draws table to bitmap */
void draw_dtable(dtable *t, ALLEGRO_BITMAP *bmp)
{
    int w = al_get_bitmap_width(bmp), h = al_get_bitmap_height(bmp);
    int n,i;

    if(w < t->xsize || h < t->ysize)
    {
        errlog("dtable doesn't fit in bitmap dimensions.");
        return;
    }

    /* Lock bitmap to draw faster
     * creating bmp with ALLEGRO_NO_PRESERVE_TEXTURE may improve performance */
    ALLEGRO_LOCKED_REGION *lr = al_lock_bitmap(bmp,
                    ALLEGRO_PIXEL_FORMAT_ABGR_8888, ALLEGRO_LOCK_READWRITE);
    pixel *ptr = (pixel *) lr->data;


    n=0;
    do
    {
        for(i=0; i<t->xsize; i++)
        {
            ptr[i] = get_col(t, n);
            n++;
        }
        /* each row has lenght pitch bytes */
        ptr += (lr->pitch)/sizeof(pixel);
    } while(n < t->xsize*t->ysize);

    al_unlock_bitmap(bmp);
}


/* draw only the boundary points
 * note: we need to draw the interior points and not the boundary ones! */
void draw_bd(pt_list *bd, ALLEGRO_BITMAP *bmp, pixel color)
{

    /* Lock bitmap to draw faster
     * creating bmp with ALLEGRO_NO_PRESERVE_TEXTURE may improve performance */
    ALLEGRO_LOCKED_REGION *lr = al_lock_bitmap(bmp,
                                               ALLEGRO_PIXEL_FORMAT_ABGR_8888, ALLEGRO_LOCK_READWRITE);
    pixel *ptr = (pixel *) lr->data;

    while(bd)
    {
        *(ptr + bd->val.y*lr->pitch/sizeof(pixel) + bd->val.x)  = color;
        bd = bd->next;
    }

    al_unlock_bitmap(bmp);
}


/* Allegro stuff */
void init_allegro_stuff(void){
    if(!al_init())
    {
        errlog("Error with al_init()");
        exit(-1);
    }
    if(!al_init_font_addon())
    {
        errlog("Error with font addon");
        exit(-1);
    }

    default_font = al_create_builtin_font();
    al_init_image_addon();
    al_init_primitives_addon();
    al_install_keyboard();
    al_install_mouse();
    queue = al_create_event_queue();
    al_register_event_source(queue,al_get_keyboard_event_source());
    al_register_event_source(queue, al_get_mouse_event_source());
}

void clear_bitmap(ALLEGRO_BITMAP *bmp, ALLEGRO_COLOR color)
{
    ALLEGRO_BITMAP *target = al_get_target_bitmap();
    al_set_target_bitmap(bmp);
    al_clear_to_color(color);
    al_set_target_bitmap(target);
}


/* if savefile is NULL, save to a sequence of numbered files */
int save_bmp(ALLEGRO_BITMAP *bmp, const char *savefile)
{
    static int count = 0;
    char filename[128];

    if(savefile)
        sprintf(filename, "%s", savefile);
    else
    {
        sprintf(filename, "step%05d.png", count);
        count++;
    }

    if(!al_save_bitmap(filename, bmp))
    {
        errlog("Error saving %s\n", filename);
        return 0;
    }

    return 1;
}

void draw_bitmap(ALLEGRO_BITMAP *bmp, ALLEGRO_DISPLAY *disp)
{
    ALLEGRO_BITMAP *target = al_get_target_bitmap();
    al_set_target_backbuffer(disp);
    al_clear_to_color(al_map_rgba(0,0,0,0));
    al_draw_bitmap(bmp,0,0,ALLEGRO_FLIP_VERTICAL);
    al_set_target_bitmap(target);
}

void flip_display()
{
    al_flip_display();
}

void sig_handler(int signo)
{
    if (signo == SIGINT)
    {
        printf("\n*** Received SIGINT. Exiting after this round *** \n");
        save_and_exit=1;
    }
}

void draw_crosshair(int x, int y, ALLEGRO_COLOR color, float stroke)
{
    al_draw_line(0, y, al_get_bitmap_width(al_get_target_bitmap()), y, color, stroke);
    al_draw_line(x, 0, x, al_get_bitmap_height(al_get_target_bitmap()), color, stroke);
//    al_draw_line(x-10, y, x+10, y, color, stroke);
//    al_draw_line(x, y-10, x, y+10, color, stroke);
}

void draw_palette()
{
    ALLEGRO_BITMAP *target = al_get_target_bitmap();
    int xs = al_get_bitmap_width(target);
    int ys = al_get_bitmap_height(target);
    int i;
    
    for(i = xs/2; i< xs; i++)
    {
        al_draw_line(i, 0, i, 30, palette[(palsize*i*2)/xs-palsize],1);
    }
}


/*
 f(x) = (1-x^2)(1+(k/2+1)x^2) has f(-1) = f(1) = 0, f'(0)=0, f''(0) = k.
*/
//#define PHI_1 PHI_2
//
//double PHI_2(double x)
//{
//    x = x-floor(x);
//    if(x < .5)
//    {
//        x -= .25;
//        return (1-16*x*x)*(1+(16-2*pi*pi)*x*x);
//    }
//
//    x-= .75;
//    return -(1-16*x*x)*(1+(16-2*pi*pi)*x*x);
//}

#define PHI_1 PHI
#define PHI_2 PHI

void draw_port_fast(double a, double b, int pts, int iter, ALLEGRO_BITMAP *bmp)
{
    
    double x,y;
    double xx, yy;
    int i;
    int xsize = al_get_bitmap_width(aux_port_bmp);
    int ysize = al_get_bitmap_height(aux_port_bmp);
    float dx = 1.0/(pts+1);
    float dy = 1.0/(pts+1);
    
    al_set_target_bitmap(aux_port_bmp);
    al_clear_to_color(port_bg_color);
    al_lock_bitmap(aux_port_bmp, ALLEGRO_PIXEL_FORMAT_ABGR_8888, ALLEGRO_LOCK_READWRITE);
    
    for(x=dx; x<.5; x+=dx)
    {
        for(y=dy; y<.5; y+=dy)
        {
            xx = x;
            yy = y;
            ALLEGRO_COLOR color = colorize_port ?
            (palette ? palette[(int)((ctype_y[colorize_port]*2*y+ctype_x[colorize_port]*2*x)*palsize)%palsize] : al_map_rgb(2*x*255, 2*y*255, 50)) :
            al_map_rgba_f(1-port_bg_color.r,1-port_bg_color.g, 1-port_bg_color.b, 1);
            
            for(i=0; i<iter; i++)
            {
                yy += b*PHI_2(xx);
                xx += a*PHI_1(yy);
                xx = xx-floor(xx);
                yy = yy-floor(yy);
                
                // use the symmetries to look at 1/4 of the phase space
                if(xx > .5)
                {
                    xx = yy < .5 ? xx - .5 : 1-xx;
                    yy = yy < .5 ? .5 - yy : 1 - yy;
                }
                
                if(yy > .5)
                {
                    yy = yy - .5;
                    xx = .5 - xx;
                }
                
                if(i > iter*iter_discard)
                {
                    al_put_pixel(2*xx*xsize, 2*yy*ysize, blend_colors ? color_blend(color, al_get_pixel(aux_port_bmp, 2*xx*xsize, 2*yy*ysize), 0.8) : color);
                }
                
            }
        }
    }
    
    al_unlock_bitmap(aux_port_bmp);
    al_set_target_bitmap(bmp);
    al_clear_to_color(port_bg_color);
    al_draw_bitmap(aux_port_bmp, 0,0,ALLEGRO_FLIP_VERTICAL | ALLEGRO_FLIP_HORIZONTAL);
    al_draw_bitmap(aux_port_bmp, xsize, 0, ALLEGRO_FLIP_HORIZONTAL);
    al_draw_bitmap(aux_port_bmp, 0, ysize, ALLEGRO_FLIP_VERTICAL);
    al_draw_bitmap(aux_port_bmp, xsize, ysize,0);
}

void draw_port(double a, double b, int pts, int iter, ALLEGRO_BITMAP *bmp)
{
    if(draw_fast)
    {
        draw_port_fast(a,b,pts,iter,bmp);
        return;
    }

    double x,y;
    double xx, yy;
    int i;
    int xsize = al_get_bitmap_width(bmp);
    int ysize = al_get_bitmap_height(bmp);
    float dx = 1.0/(pts+1);
    float dy = 1.0/(pts+1);
    al_set_target_bitmap(bmp);
    al_clear_to_color(port_bg_color);
    al_lock_bitmap(bmp, ALLEGRO_PIXEL_FORMAT_ABGR_8888, ALLEGRO_LOCK_READWRITE);
    
    
    for(x=dx; x<1; x+=dx)
    {
        for(y=dy; y<1; y+=dy)
        {
            xx = x;
            yy = y;
            ALLEGRO_COLOR color = colorize_port ?
            (palette ? palette[(int)((ctype_y[colorize_port]*y+ctype_x[colorize_port]*x)*palsize)%palsize] : al_map_rgb(x*255, y*255, 50)) :
            al_map_rgba_f(1-port_bg_color.r,1-port_bg_color.g, 1-port_bg_color.b, 1);
            
            for(i=0; i<iter; i++)
            {
                yy += b*PHI_2(xx);
                xx += a*PHI_1(yy);
                xx = xx-floor(xx);
                yy = yy-floor(yy);
                if(i > iter*iter_discard)
                {
                    al_put_pixel(xx*xsize, (1-yy)*ysize, blend_colors ? color_blend(color, al_get_pixel(bmp, xx*xsize, (1-yy)*ysize), 0.8) : color);
                }
            }
        }
    }
    
    // temporary
    // vertical way of finding the shearless KAM circle
    if(draw_shearless > 1)
    {
        //version vertical
        xx = a/2-.25;
        yy = 0.25;
        for(i=0; i<5*iter; i++)
        {
            yy += b*PHI_2(xx);
            xx += a*PHI_1(yy);
            xx = xx-floor(xx);
            yy = yy-floor(yy);
            if(i > iter*iter_discard)
            {
                al_put_pixel(xx*xsize, (1-yy)*ysize, al_map_rgb(255,255,255));
            }
        }
    }

    // horizontal way of finding it
    if(draw_shearless > 0)
    {
        //version horizontal
        xx = .25;
        yy = -b/2+.25;
        for(i=0; i<5*iter; i++)
        {
            yy += b*PHI_2(xx);
            xx += a*PHI_1(yy);
            xx = xx-floor(xx);
            yy = yy-floor(yy);
            if(i > iter*iter_discard)
            {
                al_put_pixel(xx*xsize, (1-yy)*ysize, al_map_rgb(255,255,255));
            }
        }
    }
    
    
    al_unlock_bitmap(bmp);
}


void save_port(ALLEGRO_BITMAP *bmp, double x, double y)
{
    char filename[128];
    sprintf(filename, "%1.4f_%1.4f.png", x, y);
    if(al_save_bitmap(filename, bmp))
        printf("Saved image %s.\n", filename);
    else
        printf("Error saving %s.\n", filename);
    
}

void print_help(int i)
{
    
    
    if(i==0)
    {
        printf("Usage: viewer [-opts] dtable.tbl\n"
           "Options:\n\n"
           "-n [sample points]\n"
           "-i [iterations]\n"
           "-r [portrait resolution (horizontal)]\n"
           "-c [0:1] colorize phase space?\n"
           "-b use white instead of black portrait background\n"
           "-z [scale factor] rescale portrait display by factor\n"
           "-p [file] load palette file\n");
    }
    
    printf(
           "\nKeys:\n"
           "h:     show help\n"
           "Space: lock/unlok coordinates\n"
           "arrows move locked point by 0.001\n"
           "shift+arrows move locked point by 0.0001\n"
           "[ / ]  decrease/increase sample points x 2 (when locked)\n"
           "- / =  decrease/increase iter x 2 (when locked)\n"
           ", / .  cycle palettes\n"
           "s:     save current portrait image\n"
           "o:     toggle portrait color\n"
           "b:     toggle color blending\n"
           "t      toggle shearless orbit\n"
           "Q/esc  exit\n"
           );

}
int main(int argc, char **argv)
{
    init_allegro_stuff();
    dtable *t=NULL;
    int xsize = 500, ysize = 500;
    int opt;

    int sample_points = 20;
    int iter = 1000;
    int port_size = 500;
    int load_flag =0;
    char *dtable_file = "standard.tbl";
    char *image_file = "standard.png";
    char *palette_file = NULL;
    char pal_file_name[128];
    char *palette_dir = "palettes";
    
    /* Parse args and initialize data */
    while ((opt = getopt(argc, argv, "hd:p:bz:c:i:n:r:")) != -1)
    {
        switch(opt)
        {
            case 'n': sample_points = atoi(optarg); break;
            case 'i': iter = atoi(optarg); break;
            case 'r': port_size = atoi(optarg); break;
            case 'c': colorize = atoi(optarg); break;
            case 'b': port_bg_color = al_map_rgb(255,255,255); break;
            case 'z': port_scale = atof(optarg); break;
            case 'p': palette_file = optarg; break;
            case 'd': palette_dir = optarg; break;
            case 'h':
            default:
                print_help(0);
                exit(0);
        }
        
    }

    if(optind < argc) dtable_file = argv[optind];

    /* Finished parsing args */

    printf("iter = %d, sample_points = %d, port_size = %dx%d\n",
            iter, sample_points, port_size, port_size);
    printf("Loading %s\n", dtable_file);
    t = load_dtable(dtable_file);
    if(!t) return -1;
    dtable_print_params(t);
    xsize = t->xsize;
    ysize = t->ysize;
    
    /* read palette names if there are any */
    pal_num=0;
    if(al_filename_exists(palette_dir))
    {
        ALLEGRO_FS_ENTRY* dir = al_create_fs_entry(palette_dir);
        ALLEGRO_FS_ENTRY* file;
        if(al_open_directory(dir))
        {
            while( (file = al_read_directory(dir)) )
            {
                pal_file[pal_num]=malloc(MAX_PATH_LENGTH*sizeof(char));
                sprintf(pal_file[pal_num],"%s", al_get_fs_entry_name(file));
                printf("Found %s\n", pal_file[pal_num]);
                if(!strcmp(strrchr(pal_file[pal_num],'.'), ".map")) pal_num++;
            }
        }
        al_destroy_fs_entry(dir);
    }
    pal_on=pal_num-1;
    

    if(palette_file) palette = load_palette_al(palette_file, palsize);
    /* initialize display */
    ALLEGRO_DISPLAY *disp = al_create_display(xsize,ysize);
    ALLEGRO_DISPLAY *disp2 = al_create_display(port_size*port_scale, port_size*port_scale);
 
    al_set_target_backbuffer(disp2);
    ALLEGRO_BITMAP *port_bmp = al_create_bitmap(port_size,port_size);
    aux_port_bmp = al_create_bitmap(port_size/2, port_size/2);
    
    al_set_target_bitmap(aux_port_bmp);
    al_clear_to_color(port_bg_color);
    
    al_set_target_bitmap(port_bmp);
    al_clear_to_color(port_bg_color);

    al_set_target_backbuffer(disp);
    ALLEGRO_BITMAP *buffer = al_create_bitmap(xsize, ysize);
    clear_bitmap(buffer, al_map_rgba(0,0,0,0));
    draw_dtable(t, buffer);
    draw_bitmap(buffer, disp);
    flip_display();


    int redraw = 1, noexit = 1, draw_portrait=0;
    int mx = 0, my = 0, mbdown = 0, mbdown_x = 0, mbdown_y = 0;
    ALLEGRO_EVENT ev;
    double old_time = al_current_time();
    double fixed_dt = 1/60.0;
    float port_dt = 0.1;
    double port_x=0, port_y=0;
    double port_time = al_current_time();
    int lock_port = 0;
    ALLEGRO_DISPLAY * focus = NULL;
    int iter_lock = iter, sample_points_lock = sample_points;
    int pal_inc = 0;
    
    while(noexit)
    {
        double dt = al_current_time() - old_time;
        al_rest(fixed_dt - dt); //rest at least fixed_dt
        dt = al_get_time() - old_time;
        old_time = al_get_time();

        while(al_get_next_event(queue, &ev)){ // empty out the event queue
            switch(ev.type){
                case ALLEGRO_EVENT_DISPLAY_CLOSE:
                    noexit = 0;
                    redraw = 0;
                    break;
                case ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY:
                    focus = ev.mouse.display;
                    if(focus == disp)
                        al_hide_mouse_cursor(disp);
                    break;
                case ALLEGRO_EVENT_MOUSE_LEAVE_DISPLAY:
                    if(focus == disp)
                        al_show_mouse_cursor(disp);
                    focus = NULL;
                    break;
                case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
                    if(mbdown || focus != disp) break;
                    if(ev.mouse.button==2)
                    {
                        lock_port = 0;
                        break;
                    }
                    mbdown_x = ev.mouse.x;
                    mbdown_y = ev.mouse.y;
                    mbdown = 1;
                    redraw = 1;
                    draw_portrait = 1;
                    lock_port = 1;
                    port_x = xtoa(t, mbdown_x);
                    port_y = ytob(t, ysize-1-mbdown_y);
                    break;

                case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                    mbdown = 0;
                    redraw = 1;
                    break;

                case ALLEGRO_EVENT_MOUSE_AXES:
                    mx = ev.mouse.x;
                    my = ev.mouse.y;
                    redraw = 1;
                    if(old_time - port_time > port_dt && !lock_port)
                    {
                        draw_portrait=1;
                        port_x = xtoa(t, mx);
                        port_y = ytob(t, ysize-1-my);
                    }
                    break;
                case ALLEGRO_EVENT_KEY_CHAR:
                    switch(ev.keyboard.keycode)
                    {
                        case ALLEGRO_KEY_ESCAPE:
                        case ALLEGRO_KEY_Q:
                            noexit = 0;
                            break;
                        case ALLEGRO_KEY_H:
                            print_help(1);
                            break;
                        case ALLEGRO_KEY_SPACE:
                            if(lock_port)
                                lock_port = 0;
                            else
                            {
                                lock_port = 1;
                                mbdown_x = mx;
                                mbdown_y = my;
                                draw_portrait=1;
                                redraw=1;
                            }
                            break;
                        case ALLEGRO_KEY_LEFT:
                            port_x -= ev.keyboard.modifiers & ALLEGRO_KEYMOD_SHIFT ? 0.0001 : 0.001;
                            draw_portrait = redraw = 1;
                            break;
                        case ALLEGRO_KEY_RIGHT:
                            port_x += ev.keyboard.modifiers & ALLEGRO_KEYMOD_SHIFT ? 0.0001 : 0.001;
                            draw_portrait = redraw = 1;
                            break;
                        case ALLEGRO_KEY_UP:
                            port_y += ev.keyboard.modifiers & ALLEGRO_KEYMOD_SHIFT ? 0.0001 : 0.001;
                            draw_portrait = redraw = 1;
                            break;
                        case ALLEGRO_KEY_DOWN:
                            port_y -= ev.keyboard.modifiers & ALLEGRO_KEYMOD_SHIFT ? 0.0001 : 0.001;
                            draw_portrait = redraw = 1;
                            break;
                        case ALLEGRO_KEY_EQUALS:
                            iter_lock *= 2;
                            printf("iter_lock = %d\n", iter_lock);
                            draw_portrait = 1;
                            break;
                        case ALLEGRO_KEY_MINUS:
                            iter_lock = max(iter_lock/2, 10);
                            printf("iter_lock = %d\n", iter_lock);
                            draw_portrait=1;
                            break;
                        case ALLEGRO_KEY_OPENBRACE:
                            sample_points_lock = max(sample_points_lock/2, 1);
                            printf("sample_points_lock = %d x %d\n", sample_points_lock, sample_points_lock);
                            draw_portrait=1;
                            break;
                        case ALLEGRO_KEY_CLOSEBRACE:
                            sample_points_lock *= 2;
                            printf("sample_points_lock = %d x %d\n", sample_points_lock, sample_points_lock);
                            draw_portrait=1;
                            break;
                        case ALLEGRO_KEY_C:
                            SWITCH(colorize);
                            clear_bitmap(buffer, al_map_rgba(0,0,0,0));
                            draw_dtable(t, buffer);
                            redraw=1;
                            break;
                        case ALLEGRO_KEY_O:
                            colorize_port = (colorize_port+1) % 4;
                            printf("colorize_port = %d\n", colorize_port);
                            draw_portrait=1;
                            break;
                        case ALLEGRO_KEY_S:
                            save_port(port_bmp, port_x, port_y);
                            break;
                        case ALLEGRO_KEY_B:
                            SWITCH(blend_colors);
                            printf("blend_colors = %d\n", blend_colors);
                            draw_portrait=1;
                            break;
                        case ALLEGRO_KEY_F:
                            SWITCH(draw_fast);
                            printf("draw_fast = %d\n", draw_fast);
                            draw_portrait=1;
                            break;
                        case KEY_PAL:
                            pal_inc=1;
                        case KEY_PALM:
                            if(ev.keyboard.keycode==KEY_PALM)
                                pal_inc=-1;
                            if(pal_num>0){
                                pal_on = pmod((pal_on+pal_inc),pal_num);
                                if(palette) free(palette);
                                palette = load_palette_al(pal_file[pal_on], palsize);
                                strcpy(pal_file_name, pal_file[pal_on]);
                                printf("Palette %d/%d: %s", pal_on, pal_num,pal_file_name);
                                draw_portrait=1;
                                redraw = 1;
                                //make_palette_bitmap(pal_bmp);
                            }
                            break;
                        case ALLEGRO_KEY_T:
                            draw_shearless = (draw_shearless + 1) % 3;
                            printf("draw_shearless = %d", draw_shearless);
                            draw_portrait=1;
                            break;

                        break;
                    }
                    break;
            }
        }

        if(redraw)
        {
            redraw = 0;
            al_set_target_backbuffer(disp);
            draw_bitmap(buffer, disp);
            if(palette) draw_palette();
            if(focus == disp)
            {
                draw_crosshair(mx, my, al_map_rgba(255,0,0,150), 2);
                al_draw_textf(default_font, al_map_rgb(255, 0, 255), 0,0, ALLEGRO_ALIGN_LEFT, "(%g, %g) | (%g, %g)", port_x, port_y, xtoa(t, mx), 1-ytob(t, ysize-my-1));
            }
            if(lock_port) draw_crosshair(atox(t, port_x), ysize-1-btoy(t,port_y), al_map_rgba(0,255,0,200), 1);
            //al_draw_filled_circle(atox(t, port_x), ysize-1-btoy(t,port_y), 5, al_map_rgba(255, 0, 0, 100));
    
            flip_display();
        }

        if(draw_portrait && (lock_port || focus == disp))
        {
            draw_portrait = 0;
            port_time = al_current_time();
            printf("(%g, %g) : %s | (%g, %g)\n", port_x, port_y, t->ok[dti(t, port_x, port_y)] ? "interior" : "not interior", t->mx[dti(t, port_x, port_y)], t->my[dti(t, port_x, port_y)]);
            draw_port(port_x, port_y, lock_port ? sample_points_lock : sample_points
                      , lock_port ? iter_lock : iter
                      , port_bmp);
            al_set_target_backbuffer(disp2);
            al_clear_to_color(al_map_rgb(0,0,0));
            al_draw_scaled_bitmap(port_bmp, 0,0, port_size, port_size, 0, 0, port_size*port_scale, port_size*port_scale, 0);
            flip_display();
        }

    }
    return 0;
}

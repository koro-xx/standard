/*
 gcc -I/usr/local/include -L/usr/local/lib -lallegro -lallegro_main -lallegro_font -lallegro_image -lallegro_primitives cmdline.c

 TODO:
    -use MAX_OPT preprocessor var to use only y coordinate and other optimizations
    -parse args (iter_max and other stuff)
    -save on regular time intervals
    -improve choice of iter and min_new_points (optimize?)
    -maybe store only boundary list? much lighter on memory, could be raw
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include "macros.h"


/* Allegro stuff */
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
//#include <allegro5/allegro_primitives.h>
//#include <allegro5/allegro_font.h>

#define COLOR_BLACK ((ALLEGRO_COLOR){0,0,0,255})
#define COLOR_NULL ((ALLEGRO_COLOR){0,0,0,0})
#define COLOR_WHITE ((ALLEGRO_COLOR){255,255,255,255})

#define BD_COLOR ( ((pixel){255,255,255,255}) )

// ALLEGRO_FONT *default_font = NULL;

/* Our displacement function */
#define PHI(t) (sin(2.0*pi*(t)))
#define MIN_DISP 1.0

// get index corresponding to (a,b) in dtable and other transformations
#define atox(t, a) ( ((int)((t)->xsize*((a) - (t)->a0)/((t)->a1 - (t)->a0))) )
#define btoy(t, b) ( ((int)((t)->ysize*((b) - (t)->b0)/((t)->b1 - (t)->b0))) )
#define xtoa(t, x) ( (((x)*((t)->a1 - (t)->a0))/(t)->xsize + (t)->a0) )
#define ytob(t, y) ( (((y)*((t)->b1 - (t)->b0))/(t)->ysize + (t)->b0) )

#define dti_i(t, x, y) ( ((t)->xsize*(y) + x) )
#define dti(t, a, b) ( dti_i(t, atox(t, a), btoy(t, b)) )
#define itox(t, n) ( ((n) % (t)->xsize) )
#define itoy(t, n) ( ((n) / (t)->xsize) )

#define has_interior(t, a, b) ( ((t)->ok[dti(t,a,b)]) )
#define has_interior_i(t, x, y) ( ((t)->ok[dti_i(t,x,y)]) )

// #define time_sec() ((float)clock()/CLOCKS_PER_SEC);

volatile sig_atomic_t save_and_exit = 0;

typedef struct int2
{
    int x;
    int y;
} int2;

typedef struct pt_list {
    int2 val;
    struct pt_list *next;
} pt_list;

typedef struct pixel
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char a;
} pixel;

// todo: store maximizing point in dtable + level
typedef struct dtable
{
    /* pixel count */
    int xsize, ysize;
    /* auxiliary */
    int levels;
    /* real dimensions */
    double a0, b0, a1, b1;
    /* 0 if we can't say it's interior, number of iterations for interior otherwise */
    int *ok;
    /* maximizing parameters */
    // todo: we should also store the maximizing point!
    double *ma;
    double *mb;

    /* maximizing points */
    double *mx;
    double *my;
} dtable;


int pt_list_len(pt_list *head)
{
    int len = 0;

    while(head)
    {
        len++;
        head = head->next;
    }
    return len;
}

void pt_list_add(pt_list **head, int2 data)
{
    pt_list *new_node = malloc(sizeof *new_node);

    new_node->val = data;
    new_node->next = *head;
    *head = new_node;
}

void pt_list_insert(pt_list **head, pt_list *node)
{
    node->next = *head;
    *head = node;
}

void pt_list_add_unique(pt_list **head, int2 data)
{
    pt_list *walk = *head;

    while(walk)
    {
        if(walk->val.x == data.x && walk->val.y == data.y)
            return;
        walk = walk->next;
    }

    walk = malloc(sizeof(*walk));
    walk->val = data;
    walk->next = *head;
    *head = walk;
}


pt_list **pt_list_remove(pt_list **head, pt_list *node)
{
    while( (*head) != node )
        head = &(*head)->next;

    *head = node->next;
    return head;
}

void pt_list_destroy(pt_list **head)
{
    pt_list *foo = *head;

    while(*head)
    {
        foo = *head;
        *head = foo->next;
        free(foo);
    }
}

/* uniform random distribution between a and b (shitty version) */
double rand_uni(double a, double b)
{
    return (b-a)*rand()/RAND_MAX +a;
}

void destroy_dtable(dtable *t)
{
    if(!t) return;
    if(t->mx) free(t->mx);
    if(t->my) free(t->my);
    if(t->ma) free(t->ma);
    if(t->mb) free(t->mb);
    if(t->ok) free(t->ok);
    free(t);
}

struct dtable *create_dtable(void)
{
    dtable *t = calloc(1, sizeof(*t));
    return t;
}

int init_dtable (dtable *t, int xsize, int ysize)
{

    t->xsize = xsize;
    t->ysize = ysize;

    if(xsize < 0 || ysize < 0)
        goto Error;

    t->mx = calloc(xsize*ysize, sizeof(*t->mx));
    t->ma = calloc(xsize*ysize, sizeof(*t->ma));
    t->my = calloc(xsize*ysize, sizeof(*t->my));
    t->mb = calloc(xsize*ysize, sizeof(*t->mb));
    t->ok = calloc(xsize*ysize, sizeof(*t->ok));
    if(!t->mx || !t->my || !t->ma || !t->my || !t->ok) goto Error;

    return 1;

    Error:
    errlog("Error initializing %d x %d table.", xsize, ysize);
    destroy_dtable(t);
    return 0;
}

int save_dtable(dtable *dt, const char *filename)
{
    FILE *fp;
    dtable t = *dt;

    if(!dt) return 0;

    fp = fopen(filename, "wb");
    if(!fp)
    {
        errlog("Error opening file for writing");
        goto Error;
    }

    if(fwrite(&t, sizeof(t), 1, fp) != 1)
    {
        errlog("Write error");
        goto Error;
    }
    if(fwrite(t.mx, sizeof(*t.mx), t.xsize*t.ysize, fp) != t.xsize*t.ysize)
        goto Error;
    if(fwrite(t.my, sizeof(*t.my), t.xsize*t.ysize, fp) != t.xsize*t.ysize)
        goto Error;
    if(fwrite(t.ma, sizeof(*t.ma), t.xsize*t.ysize, fp) != t.xsize*t.ysize)
        goto Error;
    if(fwrite(t.mb, sizeof(*t.mb), t.xsize*t.ysize, fp) != t.xsize*t.ysize)
        goto Error;
    if(fwrite(t.ok, sizeof(*t.ok), t.xsize*t.ysize, fp) != t.xsize*t.ysize)
        goto Error;

    deblog("%s saved: %ld bytes.", filename, t.xsize*t.ysize*(2*sizeof(t.mx) + 2*sizeof(t.ma) + sizeof(t.ok) + sizeof(t)) );
    fclose(fp);
    return 1;

    Error:
    errlog("Error saving %s.", filename);
    if(fp) fclose(fp);
    return 0;
}

struct dtable *load_dtable(const char *filename)
{
    FILE *fp = NULL;
    dtable *t = NULL;

    fp = fopen(filename, "rb");
    if(!fp) goto Error;

    t = create_dtable();
    if(fread(t, sizeof(*t), 1, fp) != 1)
        goto Error;
    if(!init_dtable(t, t->xsize, t->ysize))
        goto Error;
    if(fread(t->ma, sizeof(*t->ma), t->xsize*t->ysize, fp) != t->xsize*t->ysize)
        goto Error;
    if(fread(t->mb, sizeof(*t->mb), t->xsize*t->ysize, fp) != t->xsize*t->ysize)
        goto Error;
    if(fread(t->mx, sizeof(*t->mx), t->xsize*t->ysize, fp) != t->xsize*t->ysize)
        goto Error;
    if(fread(t->my, sizeof(*t->my), t->xsize*t->ysize, fp) != t->xsize*t->ysize)
        goto Error;
    if(fread(t->ok, sizeof(*t->ok), t->xsize*t->ysize, fp) != t->xsize*t->ysize)
        goto Error;

    deblog("%s loaded: %ld bytes.", filename,
            sizeof(*t) + (2*sizeof(*t->mx) + 2*sizeof(*t->ma) + sizeof(*t->ok))*t->xsize*t->ysize);
    return t;

    Error:
    errlog("Error loading %s.", filename);
    if(fp) fclose(fp);
    destroy_dtable(t);
    return NULL;
}

int find_max_disp(dtable *t, double a, double b, double x, double y, int iter)
{
    int i,n;
    double x0 = x, y0 = y;
    float mdx = 0, mdy = 0;

    // todo: make it only for vertical speed
    for(i=0; i<iter; i++)
    {
        y += b*PHI(x);
        x += a*PHI(y);
        mdx = max(mabs(x-x0), mdx);
        mdy = max(mabs(y-y0), mdy);
        if((mdx >= MIN_DISP) && (mdy >= MIN_DISP))
            break;
    }

    if( mdx >= MIN_DISP && mdy >= MIN_DISP)
    {
        n = dti(t, a, b);

        /* check just in case */
        if(n < 0 || n >= t->xsize*t->ysize) return 0;

        if (t->ok[n]) return 1;

        /* we just found a new point, save coordinates and parameters */

        t->ok[n] = i;
        t->mx[n] = x0;
        t->my[n] = y0;
        t->ma[n] = a;
        t->mb[n] = b;
        return 1;
    }

    return 0;
}

double rand_x0(void)
{
    return rand_uni(0.01, 0.02);
}

double rand_y0(void)
{
    return rand_uni(0.01, 0.02);
}

void full_pass(dtable *t, int iter)
{
    /* do a full pass on the table. What initial points? */
    double x0, y0;
    double da, db;
    double a, b;
    int c = 0;

    da = (t->a1 - t->a0)/t->xsize;
    db = (t->b1 - t->b0)/t->ysize;

    // hack to avoid missing pixels
    da /= 2;
    db /= 2;

    for(a = t->a0; a < t->a1; a += da )
    {
        c++;
        for(b = t->b0; b < t->b1; b += db)
        {
            x0 = rand_x0();
            y0 = rand_y0();

            find_max_disp(t, a, b, x0, y0, iter);
            c++;
            if(c % 5000 == 0)
            {
                printf("\r %.2f %%, iter = %d, first pass", 100.0*(a - t->a0)/(t->a1-t->a0), iter);
                fflush(stdout);
            }
        }
    }
    printf("\n");
}


/* Second pass ignores already found interior points */
void second_pass(dtable *t, int iter)
{
    /* do a full pass on the table. What initial points? */
    double da, db;
    double a, b;
    int c = 0;

    da = (t->a1 - t->a0)/t->xsize;
    db = (t->b1 - t->b0)/t->ysize;

    for(a = t->a0+da/2; a < t->a1; a += da )
    {
        c++;
        for(b = t->b0 + db/2; b < t->b1; b += db)
        {
            if(!has_interior(t, a, b))
            {
                find_max_disp(t, a, b, rand_x0(), rand_y0(), iter);
            }
            c++;
            if(c % 5000 == 0)
            {
                printf("\r %.2f %%, iter = %d, second pass", 100.0*(a - t->a0)/(t->a1-t->a0), iter);
                fflush(stdout);
            }

        }
    }
    printf("\n");
}

/* Returns 1 if pixel (x,y) is in the boundary (from the outside) */
int is_bd(dtable *t, int x, int y)
{
    int n = dti_i(t, x, y);
    int i,j, m;
    int imin = ((x==0) ? 0 : -1);
    int imax = ((x==t->xsize - 1) ? 0 : 1);

    if(t->ok[n]) return 0;

    for(j = - 1 ; j <= 1 ; j++ )
    {
        for(i = imin; i<= imax; i++)
        {
            m = n + j*t->xsize + i;
            if( (m!=n) && (m >= 0) && (m < t->xsize*t->ysize) && t->ok[m])
                return 1;
        }
    }

    return 0;
}

/* If some neighbor of (x,y) is in the boundary, add it to the list */
void update_bd(dtable *t, pt_list **bd, int x, int y)
{
    int n = dti_i(t, x, y);
    int i,j, m = 0;
    int count =0;
    int imin = ((x==0) ? 0 : -1);
    int imax = ((x==t->xsize - 1) ? 0 : 1);

    for(j = - 1 ; j <= 1 ; j++ )
    {
        for(i = imin ; i<=imax ; i++)
        {
            m = n + j*t->xsize + i;
            if( (m!=n) && (m >= 0) && (m < t->xsize*t->ysize) )
            {
                if(!t->ok[m])
                {
                    pt_list_add_unique(bd, (int2){itox(t,m), itoy(t,m)});
                    count++;
                }
            }
        }
    }

//    deblog("Found %d new boundary points", count);
}

/* Go through boundary points, check random parameter in its pixel, if we get interior remove it from list */
/* return list of new points in interior */
pt_list *bd_pass(dtable *t, pt_list **bd, int iter)
{
    int ret = 0;
    int x,y;
    double a, b;
    pt_list **current = bd;
    pt_list *new_pts = NULL;
    pt_list *foo = NULL;

    int count = 0;

    while(*current)
    {
        x = ((*current)->val).x;
        y = ((*current)->val).y;
        a = xtoa(t, x);
        b = ytob(t, y);
        a += rand_uni(0, 1.0/(t->xsize));
        b += rand_uni(0, 1.0/(t->ysize));

        find_max_disp(t, a, b, rand_x0(), rand_y0(), iter);
        if(has_interior_i(t, x, y))
        { /* found new point */
            ret++;
            /* add old point to list */
            foo = *current;
            current = pt_list_remove(bd, *current);
            pt_list_insert(&new_pts, foo);
            update_bd(t, bd, x, y);
        }
        else
        {
            current = &(*current)->next;
        }
        count++;
        if(save_and_exit)
            return NULL;

    }
    //printf("%d boundary points, %d new\n", count, ret);

    return new_pts;
}


//int is_bd(dtable *t, int x, int y)
//{
//
//    if(has_interior_i(t,x,y)) return 0;
//
//    if(x > 0)
//    {
//        if(has_interior_i(t, x-1, y)) return 1;
//        if( (y > 0) && has_interior_i(t, x-1, y-1) ) return 1;
//        if( (y < t->ysize - 1) && has_interior_i(t, x-1, y+1) ) return 1;
//    }
//
//    if(x < t->xsize - 1)
//    {
//        if(has_interior_i(t, x+1, y)) return 1;
//        if( (y > 0) && has_interior_i(t, x+1, y-1) ) return 1;
//        if( (y < t->ysize - 1) && has_interior_i(t, x+1, y+1) ) return 1;
//    }
//
//    if( (y > 0) && has_interior_i(t, x, y-1) ) return 1;
//    if( (y < t->ysize - 1 ) && has_interior_i(t, x, y+1) ) return 1;
//
//    return 0;
//}

/* Create list of boundary points in table (from outside) */
pt_list *find_bd(dtable *t)
{

    int x,y;
    pt_list *bd = NULL;
    int count = 0;

    for(y=0; y<t->ysize; y++)
    {
        for(x=0; x<t->xsize; x++)
        {
            if(is_bd(t, x, y))
            {
                pt_list_add(&bd, (int2) {x,y});
                count++;
            }
        }
    }

    printf("Found %d boundary points.\n", count);
    return bd;
}

/* palette stuff */

pixel pixel_combine(pixel p1, pixel p2, float t)
{
    pixel r;

    if((t<0) || (t>1))
        t=t-floor(t);

    r.b=(1-t)*p1.b+t*p2.b;
    r.g=(1-t)*p1.g+t*p2.g;
    r.r=(1-t)*p1.r+t*p2.r;
    r.a=255;
    return r;
}


#define MAX_PALSIZE (256*256)
#define MAX_LINE_SIZE 128
#define PALSIZE (512)
pixel *paleta;

int load_palette(char *filename){
    char sline[MAX_LINE_SIZE];
    pixel pal_temp[MAX_PALSIZE];
    int i=0;
    FILE *pal;
    int r, g, b, palsize;
    int n = PALSIZE;
    paleta = malloc(PALSIZE*sizeof(*paleta));

    if ((pal=fopen(filename,"r")))
    {
        while( (i<MAX_PALSIZE) && (fgets(sline, MAX_LINE_SIZE-1, pal)) )
        {
            if(sscanf(sline,"%d %d %d",&r,&g,&b)==3)
                pal_temp[i].a =255; pal_temp[i].b=b; pal_temp[i].g = g; pal_temp[i].r=r; i++;
        }
        fclose(pal);
        palsize=i;
        float t=((float)palsize)/n;
        for(i=0;i<n;i++)
        {
            if(t*i +1 <=palsize)
                paleta[i] = pixel_combine(pal_temp[(int)(t*i)], pal_temp[(int)(t*i) + 1], (t*i - floor(t*i)));
            else
                paleta[i]=pal_temp[(int) t*i];
        }
        /* make sure 0 is black / transparent */
        paleta[0] = (pixel){0,0,0,0};
        return 1;
    }else
        return 0;
}

pixel get_color(dtable *t, int n)
{
    if(t->ok[n])
    {
        return (pixel){255, 0, 0, 255};
    }
    else
        return (pixel){0,0,0,0};
}


/* Assumes bmp has size t->xsize, t->ysize, draws table to bitmap */
void draw_dtable(dtable *t, ALLEGRO_BITMAP *bmp)
{
    int w = al_get_bitmap_width(bmp), h = al_get_bitmap_height(bmp);
    int n,i;

    if(w < t->xsize || h < t->ysize)
    {
        errlog("dtable doesn't fit in bitmap dimensions.");
        return;
    }

    /* Lock bitmap to draw faster
     * creating bmp with ALLEGRO_NO_PRESERVE_TEXTURE may improve performance */
    ALLEGRO_LOCKED_REGION *lr = al_lock_bitmap(bmp,
                    ALLEGRO_PIXEL_FORMAT_ABGR_8888, ALLEGRO_LOCK_READWRITE);
    pixel *ptr = (pixel *) lr->data;


    n=0;
    do
    {
        for(i=0; i<t->xsize; i++)
        {
            ptr[i] = get_color(t, n);
            n++;
        }
        /* each row has lenght pitch bytes */
        ptr += (lr->pitch)/sizeof(pixel);
    } while(n < t->xsize*t->ysize);

    al_unlock_bitmap(bmp);
}

/* Assumes bmp has size t->xsize, t->ysize, draws table to bitmap */
void draw_dtable_alt(dtable *t, ALLEGRO_BITMAP *bmp, pixel color)
{
    int w = al_get_bitmap_width(bmp), h = al_get_bitmap_height(bmp);
    int n,i;

    if(w < t->xsize || h < t->ysize)
    {
        errlog("dtable doesn't fit in bitmap dimensions.");
        return;
    }

    /* Lock bitmap to draw faster
     * creating bmp with ALLEGRO_NO_PRESERVE_TEXTURE may improve performance */
    ALLEGRO_LOCKED_REGION *lr = al_lock_bitmap(bmp,
                                               ALLEGRO_PIXEL_FORMAT_ABGR_8888, ALLEGRO_LOCK_READWRITE);
    pixel *ptr = (pixel *) lr->data;


    n=0;
    do
    {
        for(i=0; i<t->xsize; i++)
        {
            if(!ptr[i].r && !ptr[i].g && !ptr[i].b)
            {
                if(t->ok[n])
                    ptr[i] = color;
            }
            n++;
        }
        /* each row has lenght pitch bytes */
        ptr += (lr->pitch)/sizeof(pixel);
    } while(n < t->xsize*t->ysize);

    al_unlock_bitmap(bmp);
}


/* draw only the boundary points
 * note: we need to draw the interior points and not the boundary ones! */
void draw_bd(pt_list *bd, ALLEGRO_BITMAP *bmp, pixel color)
{

    /* Lock bitmap to draw faster
     * creating bmp with ALLEGRO_NO_PRESERVE_TEXTURE may improve performance */
    ALLEGRO_LOCKED_REGION *lr = al_lock_bitmap(bmp,
                                               ALLEGRO_PIXEL_FORMAT_ABGR_8888, ALLEGRO_LOCK_READWRITE);
    pixel *ptr = (pixel *) lr->data;

    while(bd)
    {
        *(ptr + bd->val.y*lr->pitch/sizeof(pixel) + bd->val.x)  = color;
        bd = bd->next;
    }

    al_unlock_bitmap(bmp);
}


/* Allegro stuff */
void init_allegro_stuff(void){
    al_init();
    // al_init_font_addon();
    // default_font = al_create_builtin_font();
    al_init_image_addon();
    // al_init_primitives_addon();
    // al_install_keyboard();
    // al_install_mouse();
    // queue = al_create_event_queue();
    // al_register_event_source(queue,al_get_keyboard_event_source());
    // al_register_event_source(queue, al_get_mouse_event_source());
}

void clear_bitmap(ALLEGRO_BITMAP *bmp, ALLEGRO_COLOR color)
{
    ALLEGRO_BITMAP *target = al_get_target_bitmap();
    al_set_target_bitmap(bmp);
    al_clear_to_color(color);
    al_set_target_bitmap(target);
}


/* if savefile is NULL, save to a sequence of numbered files */
int save_bitmap(ALLEGRO_BITMAP *bmp, const char *savefile)
{
    static int count = 0;
    char filename[128];

    if(savefile)
        sprintf(filename, "%s", savefile);
    else
        sprintf(filename, "/Users/koro/Desktop/imgtest/step%05d.png", count);

    if(!al_save_bitmap(filename, bmp))
    {
        errlog("Error saving %s\n", filename);
        return 0;
    }

    count++;
    return 1;
}

void draw_bitmap(ALLEGRO_BITMAP *bmp, ALLEGRO_DISPLAY *disp)
{
    ALLEGRO_BITMAP *target = al_get_target_bitmap();
    al_set_target_backbuffer(disp);
    al_clear_to_color(al_map_rgba(0,0,0,0));
    al_draw_bitmap(bmp,0,0,0);
    al_set_target_bitmap(target);
}

void flip_display()
{
    al_flip_display();
}

void sig_handler(int signo)
{
    if (signo == SIGINT)
    {
        printf("Received SIGINT, will dump table and exit ASAP. \n");
        save_and_exit=1;
    }
}

int main(int argc, char **argv)
{
    ALLEGRO_DISPLAY *disp;
    ALLEGRO_BITMAP *buffer, *buffer2;
    init_allegro_stuff();
    dtable *t=NULL;
    float ratio = 1;
    int xsize = 500, ysize = 500;
    int i=0, opt = 0, iter = 0;

    /* parameters */
    double a0 = 0, b0 = 0, a1 = 1, b1 = 1;

    int pix_max = 500000,
        iter_start = 200,
        iter_max = 1000000,
        min_new_points = 30,
        reps_per_level = 10,
        second_pass_factor = 4,
        bd_pass_factor = 5;

    int load_flag =0, save_flag =1;
    char *dtable_file = "standard.tbl";
    char *image_file = "standard.png";

    /* seconds */
    float   info_dt = 60,
            redraw_dt = 2,
            save_image_dt = 300,
            save_dtable_dt = 1800;

    /* attempt to catch SIGINT */
    if (signal(SIGINT, sig_handler) == SIG_ERR)
        printf("\ncan't catch SIGINT\n");


    /* Parse args and initialize data */

    while ((opt = getopt(argc, argv, "ls:p:I:r:k:i:m:b:a:")) != -1)
    {
        switch(opt)
        {
            case 'l': load_flag = 1; break;
            case 's': save_flag = atoi(optarg); break;
            case 'r': reps_per_level = atoi(optarg); break;
            case 'p': pix_max = atoi(optarg); break;
            case 'I': iter_max = atoi(optarg); break;
            case 'k': min_new_points = atoi(optarg); break;
            case 'i': iter = atoi(optarg); break;
                // todo:
                // case 'L': load_file = optarg; break;
                // case 'S': save_file = optarg; break;
        }
    }

    if(load_flag) goto Load;

    i = optind;
    if(argc - i < 4)
    {
        printf("Usage: <progname> [-options] a0 a1 b0 b1\n"
               "\nOptions:\n"
               "-s [0:1]     Save table periodically? Default = 1\n"
               "-l           Load saved table and continue (ignores most other opts)\n"
               "-p [num]     Max number of pixels. Uses best resolution matching aspect ratio.\n"
               "-I [num]     Max iterations (stops increasing after that)\n"
               "-r [num]     Reps per level\n"
               "-k [num]     Minimum number of new pixels per pass (averaged over rep)\n"
               "-i [num]     Iterations for first pass\n"
               "-m [num]     Multiplier for second pass\n"
               "-b [num]     Multiplier for boundary pass\n"
               "\n Not working:\n"// todo:
               "-o [file]    Output image file (png)\n"
               "-L [file]    Path to file to be loaded. Default = standard.tbl)\n"
               "-S [file]    Path to file savefile. Default = standard.tbl\n"
               "-P [dir]     Dir where images are stored (must exist)\n"
               );
        exit(0);
    } else
    {
        a0 = atof(argv[i++]);
        a1 = atof(argv[i++]);
        b0 = atof(argv[i++]);
        b1 = atof(argv[i++]);
    }

    while (++i < argc) printf ("--- Ignored argument: %s\n", argv[i]);

    /* Finished parsing args */

Load:
    if(load_flag)
    {
        printf("Loading %s\n", dtable_file);
        t = load_dtable(dtable_file);
        if(!t) return -1;
        xsize = t->xsize;
        ysize = t->ysize;
        if(!iter) iter = t->levels; /* if iter is already set, use it */
    }
    else
    {
        ratio = (b1-b0)/(a1-a0);
        xsize = sqrt(pix_max/ratio);
        ysize = ratio*xsize;
    }

    /* initialize display */
    disp = al_create_display(xsize,ysize);
    if(!disp)
    {
        errlog("Error creating display.");
        exit(-1);
    }

    /* deal with stupid allegro bug in macOS */
    al_resize_display(disp, xsize/2, ysize/2);

    int bmpflags = al_get_new_bitmap_flags();
    al_set_new_bitmap_flags(ALLEGRO_NO_PRESERVE_TEXTURE | bmpflags);
    buffer = al_create_bitmap(xsize, ysize);
    buffer2 = al_create_bitmap(xsize, ysize);
    clear_bitmap(buffer, al_map_rgba(0,0,0,0));
    clear_bitmap(buffer2, al_map_rgba(0,0,0,0));

    if (!iter) /* if iter is already set, don't change it */
        iter = iter_start;

    /* if t wasn't loaded from file, initialize and make preliminary runs */
    if(!load_flag)
    {
        t = create_dtable();
        t->a0 = a0;
        t->a1 = a1;
        t->b0 = b0;
        t->b1 = b1;
        init_dtable(t, xsize, ysize);
        t->levels = iter;

        /* run first full pass */
        printf("Full pass...\n");
        full_pass(t, iter);

        draw_dtable(t, buffer);
        draw_bitmap(buffer, disp);
        flip_display();

        if(!save_bitmap(buffer, image_file)) exit(-1);

        iter *= second_pass_factor;
        t->levels = iter;
        printf("Second pass...\n");
        second_pass(t, iter);

        // todo: allow multiple reps for second pass

        /* After second pass, increase iter */
        iter *= bd_pass_factor;
        t->levels=iter;

        if(save_flag)
            save_dtable(t, dtable_file);
    }

    draw_dtable(t, buffer);
    draw_bitmap(buffer, disp);
    flip_display();

    if(!save_bitmap(buffer, image_file)) exit(-1);


    /* Find the boundary */
    printf("Boundary detection...\n");
    pt_list *bd_list = find_bd(t);
    pt_list *new_pts = NULL;

//    draw_dtable(t, buffer);
//    draw_bd(bd_list, buffer, (pixel){255,255,255,255});
//    draw_bitmap(buffer, disp);
//    flip_display();

    /* Boundary pass */
    float new_t, start_t, info_t, redraw_t, save_image_t, save_dtable_t;
    new_t = start_t = info_t = redraw_t = save_image_t = save_dtable_t = time_sec();

    printf("Starting boundary pass with iter=%d\n", iter);
    while(1)
    {
        int count = 0;
        for(int j=0; j<reps_per_level; j++)
        {
            /* do a boundary pass */
            new_pts = bd_pass(t, &bd_list, iter);

            /* exit was requested */
            if(save_and_exit)
            {
                if(save_flag) save_dtable(t, dtable_file);
                draw_dtable(t, buffer2);
                save_bitmap(buffer2, image_file);
                exit(0);
            }

            /* draw newfound points in white for screen only (should disable this) */
            draw_bd(new_pts, buffer, (pixel){255,255,255,255});

            count += pt_list_len(new_pts);
            pt_list_destroy(&new_pts);

            new_t = time_sec()

            if( new_t - info_t > info_dt)
            {
                info_t = time_sec();
                printf("%*d min. bd = %d, iter =%d\n", 4, (int)((new_t-start_t)/60), pt_list_len(bd_list), iter);
            }

            if( new_t - redraw_t > redraw_dt)
            {
                redraw_t = time_sec();
                draw_bitmap(buffer, disp);
                flip_display();
            }

            if( new_t - save_image_t > save_image_dt )
            {
                save_image_t = time_sec();
                draw_dtable(t, buffer2);
                save_bitmap(buffer2, image_file);
            }

            if( save_flag && (new_t - save_dtable_t > save_dtable_dt) )
            {
                save_dtable_t = time_sec();
                printf("Saving %s...\n", dtable_file);
                save_dtable(t, dtable_file);
            }
        }
        printf("%d found in last cycle, iter = %d\n", count, iter);

        /* increase level if not finidning enough points */
        // todo: keep track of pps and update accordingly
        if(count < min_new_points*reps_per_level)
        {
            iter = min(iter_max, iter*2);
            t->levels = iter;
//          save_dtable(t, "/Users/koro/Desktop/test.tbl");
            printf("Increasing iter to %d.\n", iter);

            /* make a clean redraw */
            draw_dtable(t, buffer);
            draw_bitmap(buffer, disp);
            flip_display();
        }
    }

    return 0;
}

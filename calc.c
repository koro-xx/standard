#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "macros.h"
#include "calc.h"

/* uniform random distribution between a and b (shitty version) */
#define rand_uni(a,b) ( ((b)-(a))*rand()/RAND_MAX +(a) )

/* random choice of initial point to iterate */
#define rand_x0() (rand_uni(0.01, 0.02))
#define rand_y0() (rand_uni(0.01, 0.02))

int use_shearless = 0;

int pt_list_len(pt_list *head)
{
    int len = 0;

    while(head)
    {
        len++;
        head = head->next;
    }
    return len;
}

void pt_list_add(pt_list **head, int2 data)
{
    pt_list *new_node = malloc(sizeof *new_node);

    new_node->val = data;
    new_node->next = *head;
    *head = new_node;
}

void pt_list_insert(pt_list **head, pt_list *node)
{
    node->next = *head;
    *head = node;
}

void pt_list_add_unique(pt_list **head, int2 data)
{
    pt_list *walk = *head;

    while(walk)
    {
        if(walk->val.x == data.x && walk->val.y == data.y)
            return;
        walk = walk->next;
    }

    walk = malloc(sizeof(*walk));
    walk->val = data;
    walk->next = *head;
    *head = walk;
}


pt_list **pt_list_remove(pt_list **head, pt_list *node)
{
    while( (*head) != node )
        head = &(*head)->next;

    *head = node->next;
    return head;
}

void pt_list_destroy(pt_list **head)
{
    pt_list *foo = *head;

    while(*head)
    {
        foo = *head;
        *head = foo->next;
        free(foo);
    }
}


void destroy_dtable(dtable *t)
{
    if(!t) return;
    if(t->mx) free(t->mx);
    if(t->my) free(t->my);
    if(t->ma) free(t->ma);
    if(t->mb) free(t->mb);
    if(t->ok) free(t->ok);
}

struct dtable *create_dtable(void)
{
    dtable *t = calloc(1, sizeof(*t));
    return t;
}

int init_dtable (dtable *t, int xsize, int ysize)
{

    t->xsize = xsize;
    t->ysize = ysize;

    if(xsize < 0 || ysize < 0)
        goto Error;

    t->mx = calloc(xsize*ysize, sizeof(*t->mx));
    t->ma = calloc(xsize*ysize, sizeof(*t->ma));
    t->my = calloc(xsize*ysize, sizeof(*t->my));
    t->mb = calloc(xsize*ysize, sizeof(*t->mb));
    t->ok = calloc(xsize*ysize, sizeof(*t->ok));
    if(!t->mx || !t->my || !t->ma || !t->my || !t->ok) goto Error;

    return 1;

    Error:
    errlog("Error initializing %d x %d table.", xsize, ysize);
    return 0;
}

void dtable_print_params(dtable *t)
{
    printf("\nParams: a: [%g, %g]   b: [%g, %g]\nDimensions: %d x %d\nLevels = %d\n\n", t->a0, t->a1, t->b0, t->b1, t->xsize, t->ysize, t->levels);
}

int save_dtable(dtable *dt, const char *filename)
{
    FILE *fp;
    dtable t = *dt;

    if(!dt) return 0;

    fp = fopen(filename, "wb");
    if(!fp)
    {
        errlog("Error opening file for writing");
        goto Error;
    }

    if(fwrite(&t, sizeof(t), 1, fp) != 1)
    {
        errlog("Write error");
        goto Error;
    }
    if(fwrite(t.mx, sizeof(*t.mx), t.xsize*t.ysize, fp) != t.xsize*t.ysize)
        goto Error;
    if(fwrite(t.my, sizeof(*t.my), t.xsize*t.ysize, fp) != t.xsize*t.ysize)
        goto Error;
    if(fwrite(t.ma, sizeof(*t.ma), t.xsize*t.ysize, fp) != t.xsize*t.ysize)
        goto Error;
    if(fwrite(t.mb, sizeof(*t.mb), t.xsize*t.ysize, fp) != t.xsize*t.ysize)
        goto Error;
    if(fwrite(t.ok, sizeof(*t.ok), t.xsize*t.ysize, fp) != t.xsize*t.ysize)
        goto Error;

    float bt = (t.xsize*t.ysize*(2*sizeof(*t.mx) + 2*sizeof(*t.ma) + sizeof(*t.ok)) + sizeof(t))/1000000.0;
    printf("%s saved: %.3g MB.\n", filename, bt);

    fclose(fp);
    return 1;

    Error:
    errlog("Error saving %s.", filename);
    if(fp) fclose(fp);
    return 0;
}

struct dtable *load_dtable(const char *filename)
{
    FILE *fp = NULL;
    dtable *t = NULL;

    fp = fopen(filename, "rb");
    if(!fp) goto Error;

    t = create_dtable();
    if(fread(t, sizeof(*t), 1, fp) != 1)
        goto Error;
    if(!init_dtable(t, t->xsize, t->ysize))
        goto Error;
    if(fread(t->mx, sizeof(*t->mx), t->xsize*t->ysize, fp) != t->xsize*t->ysize)
        goto Error;
    if(fread(t->my, sizeof(*t->my), t->xsize*t->ysize, fp) != t->xsize*t->ysize)
        goto Error;
    if(fread(t->ma, sizeof(*t->ma), t->xsize*t->ysize, fp) != t->xsize*t->ysize)
        goto Error;
    if(fread(t->mb, sizeof(*t->mb), t->xsize*t->ysize, fp) != t->xsize*t->ysize)
        goto Error;
    if(fread(t->ok, sizeof(*t->ok), t->xsize*t->ysize, fp) != t->xsize*t->ysize)
        goto Error;

    float bt = (sizeof(*t) + (2*sizeof(*t->mx) + 2*sizeof(*t->ma) + sizeof(*t->ok))*t->xsize*t->ysize)/1000000.0;

    printf("%s loaded: %.3g MB.\n", filename, bt);

    return t;

    Error:
    errlog("Error loading %s.", filename);
    if(fp) fclose(fp);
    destroy_dtable(t);
    return NULL;
}

// todo (new function)
dtable *join_dtable(dtable *t0, dtable *t1)
{
    dtable *t = NULL, *foo = NULL;
    double a0, b0, a1, b1;
    int x,y;

    // do it the brute way: assume you want to join horizontally.
    a0 = max(t0->a0, t1->a0);
    a1 = min(t0->a1, t1->a1);
    if(t0->a0 > t1->a0)
    {
        foo = t0;
        t0 = t1;
        t1 = foo;
    }
    a0 = t0->a0;
    a1 = t1->a1;
    b0 = max(t0->b0, t1->b0);
    b1 = min(t0->b1, t1->b1);

    printf("%g, %g - %g, %g\n", a0, a1, b0, b1);
    if(a0 >= a1 || b0 >= b1) goto Error;

    t = create_dtable();
    t->a0 = a0;
    t->a1 = a1;
    t->b0 = b0;
    t->b1 = b1;
    t->xsize = t0->xsize+t1->xsize;
    t->ysize = min(t0->xsize, t1->ysize);

    printf("%d, %d\n", t->xsize, t->ysize);

    init_dtable(t, t->xsize, t->ysize);


    for(y=0; y<t->ysize; y++)
    {
        for(x=0; x<t->xsize; x++)
        {
            int s = 0;;
            if(x<t0->xsize)
            {
                foo = t0;
            }
            else
            {
                foo = t1;
                s = t0->xsize;
            }

            int n = dti_i(foo, x-s, y);
            int m = dti_i(t, x, y);
            t->ok[m] = foo->ok[n];
            t->mx[m] = foo->mx[n];
            t->my[m] = foo->my[n];
            t->ma[m] = foo->ma[n];
            t->mb[m] = foo->mb[n];
        }
    }

    t->levels = max(t1->levels, t0->levels);

    return t;

Error:
    destroy_dtable(t);
    printf("Cannot join tables.\n");
    return NULL;
}

int find_max_disp(dtable *t, double a, double b, double x, double y, int iter)
{
    int i,n;
    double x0 = x, y0 = y;
    float maxdx = 0, maxdy = 0;

    // todo: make it only for vertical speed
    for(i=0; i<iter; i++)
    {
        y += b*PHI(x);
        x += a*PHI(y);
        // note: we keep track of the absolute displacement (max-min) to get a proper heat map
        maxdx = max(mabs(x-x0), maxdx);
        maxdy = max(mabs(y-y0), maxdy);

        if((maxdx >= MIN_DISP) && (maxdy >= MIN_DISP))
            break;
    }

    n = dti(t, a, b);
    /* check just in case */
    if(n < 0 || n >= t->xsize*t->ysize) return 0;
    if (t->ok[n])
    {
        printf("find_max_disp() returning unexpectedly, %d, %d, %d\n", itox(t, n), itoy(t, n), n);
    }

    if( (maxdx >= MIN_DISP) && (maxdy >= MIN_DISP))
    { /* we just found a new point, save coordinates and parameters */

        t->ok[n] = i;
        t->mx[n] = x0;
        t->my[n] = y0;
        t->ma[n] = a;
        t->mb[n] = b;
        return 1;
    }
    else
    { /* point is not interior, save maximal disp in mx, my */
        t->mx[n] = max(maxdx, t->mx[n]);
        t->my[n] = max(maxdy, t->my[n]);
    }

    return 0;
}

void full_pass(dtable *t, int iter)
{
    /* do a full pass on the table. What initial points? */
    double x0, y0;
    double da, db;
    double a, b;
    int percent = 0, foo =0;

    da = (t->a1 - t->a0)/t->xsize;
    db = (t->b1 - t->b0)/t->ysize;

    // hack to avoid missing pixels
    da /= 2;
    db /= 2;

    for(a = t->a0; a < t->a1; a += da )
    {
		/* show progress */
        foo = 100*(a - t->a0)/(t->a1-t->a0);
		if(foo > percent)
		{
			percent = foo;
			printf("\r %.2d %%, iter = %d, first pass", percent, iter);
			fflush(stdout);
            if(save_and_exit) /* do not save in this case */
                exit(0);
		}

        for(b = t->b0; b < t->b1; b += db)
        {
            if(use_shearless == 1)
            {
                x0 =  .25;
                y0 = -b/2+.25;
            }
            else if(use_shearless == 2)
            {
                x0 =  a/2-.25;
                y0 = .25;
            }
            else
            {
                x0 = rand_x0();
                y0 = rand_y0();
            }
            find_max_disp(t, a, b, x0, y0, iter);
        }
    }
    printf("\n");
}


/* Second pass ignores already found interior points */
void second_pass(dtable *t, int iter)
{
    /* do a full pass on the table. What initial points? */
    double da, db;
    double a, b, aa, bb;
	int percent=0, foo = 0;

    printf("use_shearless = %d\n", use_shearless);
    da = (t->a1 - t->a0)/t->xsize;
    db = (t->b1 - t->b0)/t->ysize;

    for(a = t->a0; a < t->a1; a += da )
    {
		/* show progress */
        foo = 100*(a - t->a0)/(t->a1-t->a0);
		if(foo > percent)
		{
			percent = foo;
			printf("\r %.2d %%, iter = %d", percent, iter);
			fflush(stdout);
            if(save_and_exit) /* do not save in this case */
                exit(0);
		}

        for(b = t->b0; b < t->b1; b += db)
        {
            aa = a + rand_uni(0, da);
            bb = b + rand_uni(0, db);
            if(aa > t->a1 || bb > t->b1) continue;

            if(!has_interior(t, aa, bb))
            {
                double x0,y0;
                if(use_shearless == 1)
                {
                    x0 =  .25;
                    y0 = -b/2+.25;
                }
                else if(use_shearless == 2)
                {
                    x0 =  a/2-.25;
                    y0 = .25;
                }
                else
                {
                    x0 = rand_x0();
                    y0 = rand_y0();
                }
                find_max_disp(t, aa, bb, x0, y0, iter);
            }
        }
    }
    printf("\n");
}

/* Returns 1 if pixel (x,y) is in the boundary (from the outside) */
// todo: make only depend on ok*, xsize, ysize (not on table)
int is_bd(dtable *t, int x, int y)
{
    int n = dti_i(t, x, y);
    int i,j, m;
    int imin = ((x==0) ? 0 : -1);
    int imax = ((x==t->xsize - 1) ? 0 : 1);

    if(t->ok[n]) return 0;

    for(j = - 1 ; j <= 1 ; j++ )
    {
        for(i = imin; i<= imax; i++)
        {
            m = n + j*t->xsize + i;
            if( (m!=n) && (m >= 0) && (m < t->xsize*t->ysize) && t->ok[m])
                return 1;
        }
    }

    return 0;
}



/* If some neighbor of (x,y) is in the boundary, add it to the list */
// todo: can optimize - instead of using add_unique, check if point already
// was in bf before adding x,y. Only works if function called with new point
void update_bd(dtable *t, pt_list **bd, int x, int y)
{
    int n = dti_i(t, x, y);
    int i,j, m = 0;
    int count =0;
    int imin = ((x==0) ? 0 : -1);
    int imax = ((x==t->xsize - 1) ? 0 : 1);

    for(j = - 1 ; j <= 1 ; j++ )
    {
        for(i = imin ; i<=imax ; i++)
        {
            m = n + j*t->xsize + i;
            if( (m!=n) && (m >= 0) && (m < t->xsize*t->ysize) )
            {
                if(!t->ok[m])
                {
                    pt_list_add_unique(bd, (int2){itox(t,m), itoy(t,m)});
                    count++;
                }
            }
        }
    }
}


/* Go through boundary points, check random parameter in its pixel, if we get interior remove it from list */
/* return list of new points in interior */
pt_list *bd_pass(dtable *t, pt_list **bd, int iter)
{
    int ret = 0;
    int x,y;
    double a, b, x0, y0;
    pt_list **current = bd;
    pt_list *new_pts = NULL;
    pt_list *foo = NULL;

    int count = 0;

    while(*current)
    {
        x = ((*current)->val).x;
        y = ((*current)->val).y;
        a = xtoa(t, x);
        b = ytob(t, y);
        a += rand_uni(0, (t->a1 - t->a0)/(t->xsize));
        b += rand_uni(0, (t->b1 - t->b0)/(t->ysize));

        if(use_shearless == 1)
        {
            x0 =  .25;
            y0 = -b/2+.25;
        }
        else if(use_shearless == 2)
        {
            x0 =  a/2-.25;
            y0 = .25;
        }
        else
        {
            x0 = rand_x0();
            y0 = rand_y0();
        }
        
        find_max_disp(t, a, b, x0, y0, iter);
        if(has_interior_i(t, x, y))
        { /* found new point */
            ret++;
            /* add old point to list */
            foo = *current;
            current = pt_list_remove(bd, *current);
            pt_list_insert(&new_pts, foo);
            update_bd(t, bd, x, y);
        }
        else
        {
            current = &(*current)->next;
        }
        count++;
        if(save_and_exit)
            return NULL;

    }

    return new_pts;
}

/* Create list of boundary points in table (from outside) */
// todo: change it to take only is_ok, xsize, ysize as params
pt_list *find_bd(dtable *t)
{

    int x,y;
    pt_list *bd = NULL;
    int count = 0;

    for(y=0; y<t->ysize; y++)
    {
        for(x=0; x<t->xsize; x++)
        {
            if(is_bd(t, x, y))
            {
                pt_list_add(&bd, (int2) {x,y});
                count++;
            }
        }
    }

    return bd;
}

/***********************************************************************
 * unused functions
 ***********************************************************************/

//int num_neighbors(dtable *t, int x, int y)
//{
//    int n = dti_i(t, x, y);
//    int i,j, m;
//    int imin = ((x==0) ? 0 : -1);
//    int imax = ((x==t->xsize - 1) ? 0 : 1);
//    int ret = 0;
//
//    if(t->ok[n]) return 0;
//
//    for(j = - 1 ; j <= 1 ; j++ )
//    {
//        for(i = imin; i<= imax; i++)
//        {
//            m = n + j*t->xsize + i;
//            if( (m!=n) && (m >= 0) && (m < t->xsize*t->ysize) && t->ok[m])
//                ret++;
//        }
//    }
//
//    return ret;
//}
//
///* test version updates without using add_unique. should be faster
// when many new points are found
// It checks that the point has only 1 neighbor, so it must be new
// but this should only work if the function is strictly called for new points
// PROBLEM: if there are holes, this won't work!                     */
//void update_bd2(dtable *t, pt_list **bd, int x, int y)
//{
//    int n = dti_i(t, x, y);
//    int i,j, m = 0;
//    int count =0;
//    int imin = ((x==0) ? 0 : -1);
//    int imax = ((x==t->xsize - 1) ? 0 : 1);
//
//    for(j = - 1 ; j <= 1 ; j++ )
//    {
//        for(i = imin ; i<=imax ; i++)
//        {
//            m = n + j*t->xsize + i;
//            if( (m!=n) && (m >= 0) && (m < t->xsize*t->ysize) )
//            {
//                if(!t->ok[m] && num_neighbors(t, itox(t,m), itoy(t,m)) == 1)
//                {
//                    pt_list_add(bd, (int2){itox(t,m), itoy(t,m)});
//                    count++;
//                }
//            }
//        }
//    }
//}
//

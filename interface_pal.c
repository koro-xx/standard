/*
 gcc -Ofast -I/usr/local/include -L/usr/local/lib -lallegro -lallegro_main -lallegro_font -lallegro_image -lallegro_primitives cmdline.c

 * Version with palette and blending
 * TODO:
    - fix palsize, fix how pixel color is chosen (currently dependent on palsize)
    - try different blendings
    - try symmetry (combine with the interface_projective)
    - option to increase size for savefile
    - switch palettes on the fly
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include "macros.h"
#include "calc.h"
#include "graphics.h"
#include "mini_graphics.h"


/* Allegro stuff */
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>

ALLEGRO_EVENT_QUEUE *queue = NULL;
ALLEGRO_FONT *default_font = NULL;

volatile sig_atomic_t save_and_exit = 0;
int colorize = 1;
int colorize_port = 1;
float iter_discard = 0.2;
int blend_colors = 0;
ALLEGRO_COLOR port_bg_color = (ALLEGRO_COLOR){0,0,0,255};
ALLEGRO_COLOR *palette = NULL;
int palsize = 255*255;

float port_scale = 2;

pixel get_col(dtable *t, int n)
{
    if(!t->ok[n] && itox(t, n) >= itoy(t, n))
        return colorize ? COLOR_RGBA((int)(t->my[n]*2*255), 0, 0, 255) : COLOR_BLACK;
    else
        return COLOR_WHITE;
}

ALLEGRO_COLOR color_blend(ALLEGRO_COLOR p1, ALLEGRO_COLOR p2, float t)
{
    
    if((t<0) || (t>1))
        t=t-floor(t);
     return (ALLEGRO_COLOR) {sqrt((1-t)*p1.b*p1.b+t*p2.b*p2.b), sqrt((1-t)*p1.g*p1.g+t*p2.g*p2.g), sqrt((1-t)*p1.r*p1.r+t*p2.r*p2.r), 1};
//    return (ALLEGRO_COLOR) {(1-t)*p1.b+t*p2.b, (1-t)*p1.g+t*p2.g, (1-t)*p1.r+t*p2.r, 1};
}


#define MAX_PALSIZE (256*256)
#define MAX_LINE_SIZE 128

ALLEGRO_COLOR *load_palette_al(const char *filename, int palsize){
    char sline[MAX_LINE_SIZE];
    ALLEGRO_COLOR pal_temp[MAX_PALSIZE];
    int i=0;
    FILE *pal;
    int r, g, b;
    int n = palsize;
    ALLEGRO_COLOR *paleta = NULL;
    
    pal = fopen(filename,"r");
    if(!pal) goto Error;
    
    paleta = malloc(palsize*sizeof(*paleta));
    while( (i<MAX_PALSIZE) && (fgets(sline, MAX_LINE_SIZE-1, pal)) )
    {
        if(sscanf(sline,"%d %d %d",&r,&g,&b)==3)
            pal_temp[i].a = 1; pal_temp[i].b=b/255.0; pal_temp[i].g = g/255.0; pal_temp[i].r=r/255.0; i++;
    }
    fclose(pal);
    palsize=i;
    if(palsize < 2) goto Error;
    
    float t=((float)palsize)/n;
    for(i=0;i<n;i++)
    {
        if(t*i +1 <=palsize)
            paleta[i] = color_blend(pal_temp[(int)(t*i)], pal_temp[(int)(t*i) + 1], (t*i - floor(t*i)));
        else
            paleta[i]=pal_temp[(int) t*i];
    }

    return paleta;
    
Error:
    if(paleta) free(paleta);
    return NULL;
    
}


/* Assumes bmp has size t->xsize, t->ysize, draws table to bitmap */
void draw_dtable(dtable *t, ALLEGRO_BITMAP *bmp)
{
    int w = al_get_bitmap_width(bmp), h = al_get_bitmap_height(bmp);
    int n,i;

    if(w < t->xsize || h < t->ysize)
    {
        errlog("dtable doesn't fit in bitmap dimensions.");
        return;
    }

    /* Lock bitmap to draw faster
     * creating bmp with ALLEGRO_NO_PRESERVE_TEXTURE may improve performance */
    ALLEGRO_LOCKED_REGION *lr = al_lock_bitmap(bmp,
                    ALLEGRO_PIXEL_FORMAT_ABGR_8888, ALLEGRO_LOCK_READWRITE);
    pixel *ptr = (pixel *) lr->data;


    n=0;
    do
    {
        for(i=0; i<t->xsize; i++)
        {
            ptr[i] = get_col(t, n);
            n++;
        }
        /* each row has lenght pitch bytes */
        ptr += (lr->pitch)/sizeof(pixel);
    } while(n < t->xsize*t->ysize);

    al_unlock_bitmap(bmp);
}


/* draw only the boundary points
 * note: we need to draw the interior points and not the boundary ones! */
void draw_bd(pt_list *bd, ALLEGRO_BITMAP *bmp, pixel color)
{

    /* Lock bitmap to draw faster
     * creating bmp with ALLEGRO_NO_PRESERVE_TEXTURE may improve performance */
    ALLEGRO_LOCKED_REGION *lr = al_lock_bitmap(bmp,
                                               ALLEGRO_PIXEL_FORMAT_ABGR_8888, ALLEGRO_LOCK_READWRITE);
    pixel *ptr = (pixel *) lr->data;

    while(bd)
    {
        *(ptr + bd->val.y*lr->pitch/sizeof(pixel) + bd->val.x)  = color;
        bd = bd->next;
    }

    al_unlock_bitmap(bmp);
}


/* Allegro stuff */
void init_allegro_stuff(void){
    if(!al_init())
    {
        errlog("Error with al_init()");
        exit(-1);
    }
    if(!al_init_font_addon())
    {
        errlog("Error with font addon");
        exit(-1);
    }

    default_font = al_create_builtin_font();
    al_init_image_addon();
    al_init_primitives_addon();
    al_install_keyboard();
    al_install_mouse();
    queue = al_create_event_queue();
    al_register_event_source(queue,al_get_keyboard_event_source());
    al_register_event_source(queue, al_get_mouse_event_source());
}

void clear_bitmap(ALLEGRO_BITMAP *bmp, ALLEGRO_COLOR color)
{
    ALLEGRO_BITMAP *target = al_get_target_bitmap();
    al_set_target_bitmap(bmp);
    al_clear_to_color(color);
    al_set_target_bitmap(target);
}


/* if savefile is NULL, save to a sequence of numbered files */
int save_bmp(ALLEGRO_BITMAP *bmp, const char *savefile)
{
    static int count = 0;
    char filename[128];

    if(savefile)
        sprintf(filename, "%s", savefile);
    else
    {
        sprintf(filename, "step%05d.png", count);
        count++;
    }

    if(!al_save_bitmap(filename, bmp))
    {
        errlog("Error saving %s\n", filename);
        return 0;
    }

    return 1;
}

void draw_bitmap(ALLEGRO_BITMAP *bmp, ALLEGRO_DISPLAY *disp)
{
    ALLEGRO_BITMAP *target = al_get_target_bitmap();
    al_set_target_backbuffer(disp);
    al_clear_to_color(al_map_rgba(0,0,0,0));
    al_draw_bitmap(bmp,0,0,ALLEGRO_FLIP_VERTICAL);
    al_set_target_bitmap(target);
}

void flip_display()
{
    al_flip_display();
}

void sig_handler(int signo)
{
    if (signo == SIGINT)
    {
        printf("\n*** Received SIGINT. Exiting after this round *** \n");
        save_and_exit=1;
    }
}

void draw_crosshair(int x, int y, ALLEGRO_COLOR color, float stroke)
{
    al_draw_line(x-10, y, x+10, y, color, stroke);
    al_draw_line(x, y-10, x, y+10, color, stroke);
}

void draw_palette()
{
    ALLEGRO_BITMAP *target = al_get_target_bitmap();
    int xs = al_get_bitmap_width(target);
    int ys = al_get_bitmap_height(target);
    int i;
    
    for(i = xs/2; i< xs; i++)
    {
        al_draw_line(i, 0, i, 30, palette[(palsize*i*2)/xs-palsize],1);
    }
}


//#define PHI(t) (sin(2*pi*t))
void draw_port(double a, double b, int pts, int iter, ALLEGRO_BITMAP *bmp)
{
    double x,y;
    double xx, yy;
    int i;
    int xsize = al_get_bitmap_width(bmp);
    int ysize = al_get_bitmap_height(bmp);
    float dx = 1.0/(pts+1);
    float dy = 1.0/(pts+1);
    al_set_target_bitmap(bmp);
    al_clear_to_color(port_bg_color);
    al_lock_bitmap(bmp, ALLEGRO_PIXEL_FORMAT_ABGR_8888, ALLEGRO_LOCK_READWRITE);

    for(x=dx; x<1; x+=dx)
    {
        for(y=dy; y<1; y+=dy)
        {
            xx = x;
            yy = y;
            ALLEGRO_COLOR color = colorize_port ?
            (palette ? palette[(int)((y+x)*palsize)%palsize] : al_map_rgb(x*255, y*255, 50)) :
                al_map_rgba_f(1-port_bg_color.r,1-port_bg_color.g, 1-port_bg_color.b, 1);

            for(i=0; i<iter; i++)
            {
                yy += b*PHI(xx);
                xx += a*PHI(yy);
                xx = xx-floor(xx);
                yy = yy-floor(yy);
                if(i > iter*iter_discard)
                {
                    al_put_pixel(xx*xsize, (1-yy)*ysize, blend_colors ? color_blend(color, al_get_pixel(bmp, xx*xsize, (1-yy)*ysize), 0.8) : color);
                }
            }
        }
    }

    al_unlock_bitmap(bmp);
}

void save_port(ALLEGRO_BITMAP *bmp, double x, double y)
{
    char filename[128];
    sprintf(filename, "%1.4f_%1.4f.png", x, y);
    if(al_save_bitmap(filename, bmp))
        printf("Saved image %s.\n", filename);
    else
        printf("Error saving %s.\n", filename);
    
}

int main(int argc, char **argv)
{
    init_allegro_stuff();
    dtable *t=NULL;
    int xsize = 500, ysize = 500;
    int opt;

    int sample_points = 20;
    int iter = 1000;
    int port_size = 500;
    int load_flag =0;
    char *dtable_file = "standard.tbl";
    char *image_file = "standard.png";
    char *palette_file = NULL;

    /* Parse args and initialize data */
    while ((opt = getopt(argc, argv, "p:bz:c:i:n:r:")) != -1)
    {
        switch(opt)
        {
            case 'n': sample_points = atoi(optarg); break;
            case 'i': iter = atoi(optarg); break;
            case 'r': port_size = atoi(optarg); break;
            case 'c': colorize = atoi(optarg); break;
            case 'b': port_bg_color = al_map_rgb(255,255,255); break;
            case 'z': port_scale = atof(optarg); break;
            case 'p': palette_file = optarg; break;
            default:
                printf("Usage: viewer [-opts] dtable.tbl\n"
                       "Options:\n\n"
                       "-n [sample points]\n"
                       "-i [iterations]\n"
                       "-r [portrait resolution (horizontal)]\n"
                       "-c [0:1] colorize phase space?\n"
                       "-b use white instead of black portrait background\n"
                       "-z [scale factor] rescale portrait display by factor\n"
                       "\nKeys:\n"
                       "Space: lock/unlok coordinate\n"
                       "S:     save current portrait image\n"
                       "O:     toggle portrait color\n"
                       "[ / ]  decrease/increase sample points x 2\n"
                       "- / =  decrease/increase iter x 2\n"
                       "Q/esc  exit\n"
                       );
                
        }
        
    }

    if(optind < argc) dtable_file = argv[optind];

    /* Finished parsing args */

    printf("iter = %d, sample_points = %d, port_size = %dx%d\n",
            iter, sample_points, port_size, port_size);
    printf("Loading %s\n", dtable_file);
    t = load_dtable(dtable_file);
    if(!t) return -1;
    dtable_print_params(t);
    xsize = t->xsize;
    ysize = t->ysize;

    if(palette_file) palette = load_palette_al(palette_file, palsize);
    /* initialize display */
    ALLEGRO_DISPLAY *disp = al_create_display(xsize,ysize);
    ALLEGRO_DISPLAY *disp2 = al_create_display(port_size*port_scale, port_size*port_scale);
 
    al_set_target_backbuffer(disp2);
    ALLEGRO_BITMAP *port_bmp = al_create_bitmap(port_size,port_size);
    al_set_target_bitmap(port_bmp);
    al_clear_to_color(port_bg_color);

    al_set_target_backbuffer(disp);
    ALLEGRO_BITMAP *buffer = al_create_bitmap(xsize, ysize);
    clear_bitmap(buffer, al_map_rgba(0,0,0,0));
    draw_dtable(t, buffer);
    draw_bitmap(buffer, disp);
    flip_display();


    int redraw = 1, noexit = 1, draw_portrait=0;
    int mx = 0, my = 0, mbdown = 0, mbdown_x = 0, mbdown_y = 0;
    ALLEGRO_EVENT ev;
    double old_time = al_current_time();
    double fixed_dt = 1/60.0;
    float port_dt = 0.1;
    double port_x=0, port_y=0;
    double port_time = al_current_time();
    int lock_port = 0;
    ALLEGRO_DISPLAY * focus = NULL;
    int iter_lock = iter, sample_points_lock = sample_points;
    
    while(noexit)
    {
        double dt = al_current_time() - old_time;
        al_rest(fixed_dt - dt); //rest at least fixed_dt
        dt = al_get_time() - old_time;
        old_time = al_get_time();

        while(al_get_next_event(queue, &ev)){ // empty out the event queue
            switch(ev.type){
                case ALLEGRO_EVENT_DISPLAY_CLOSE:
                    noexit = 0;
                    redraw = 0;
                    break;
                case ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY:
                    focus = ev.mouse.display;
                    break;
                case ALLEGRO_EVENT_MOUSE_LEAVE_DISPLAY:
                    focus = NULL;
                    break;
                case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
                    if(mbdown) break;
                    if(ev.mouse.button==2)
                    {
                        lock_port = 0;
                        break;
                    }
                    mbdown_x = ev.mouse.x;
                    mbdown_y = ev.mouse.y;
                    mbdown = 1;
                    redraw = 1;
                    draw_portrait = 1;
                    lock_port = 1;
                    port_x = xtoa(t, mbdown_x);
                    port_y = ytob(t, ysize-1-mbdown_y);
                    break;

                case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                    mbdown = 0;
                    redraw = 1;
                    break;

                case ALLEGRO_EVENT_MOUSE_AXES:
                    mx = ev.mouse.x;
                    my = ev.mouse.y;
                    redraw = 1;
                    if(old_time - port_time > port_dt && !lock_port)
                    {
                        draw_portrait=1;
                        port_x = xtoa(t, mx);
                        port_y = ytob(t, ysize-1-my);
                    }
                    break;
                case ALLEGRO_EVENT_KEY_CHAR:
                    switch(ev.keyboard.keycode)
                    {
                        case ALLEGRO_KEY_ESCAPE:
                        case ALLEGRO_KEY_Q:
                            noexit = 0;
                            break;
                        case ALLEGRO_KEY_SPACE:
                            if(lock_port)
                                lock_port = 0;
                            else
                            {
                                lock_port = 1;
                                mbdown_x = mx;
                                mbdown_y = my;
                                draw_portrait=1;
                                redraw=1;
                            }
                            break;
                        case ALLEGRO_KEY_LEFT:
                            port_x -= 0.001;
                            draw_portrait = redraw = 1;
                            break;
                        case ALLEGRO_KEY_RIGHT:
                            port_x += 0.001;
                            draw_portrait = redraw = 1;
                            break;
                        case ALLEGRO_KEY_UP:
                            port_y += 0.001;
                            draw_portrait = redraw = 1;
                            break;
                        case ALLEGRO_KEY_DOWN:
                            port_y -= 0.001;
                            draw_portrait = redraw = 1;
                            break;
                        case ALLEGRO_KEY_EQUALS:
                            iter_lock*=2;
                            draw_portrait = 1;
                            break;
                        case ALLEGRO_KEY_MINUS:
                            iter_lock = max(iter_lock/2, 10);
                            draw_portrait=1;
                            break;
                        case ALLEGRO_KEY_OPENBRACE:
                            sample_points_lock = max(sample_points_lock/2, 1);
                            draw_portrait=1;
                            break;
                        case ALLEGRO_KEY_CLOSEBRACE:
                            sample_points_lock *= 2;
                            draw_portrait=1;
                            break;
                        case ALLEGRO_KEY_C:
                            SWITCH(colorize);
                            clear_bitmap(buffer, al_map_rgba(0,0,0,0));
                            draw_dtable(t, buffer);
                            redraw=1;
                            break;
                        case ALLEGRO_KEY_O:
                            colorize_port = (colorize_port+1)%2;
                            printf("colorize_port = %d\n", colorize_port);
                            draw_portrait=1;
                            break;
                        case ALLEGRO_KEY_S:
                            save_port(port_bmp, port_x, port_y);
                            break;
                        case ALLEGRO_KEY_B:
                            SWITCH(blend_colors);
                            draw_portrait=1;
                            break;
                        break;
                    }
                    break;
            }
        }

        if(redraw)
        {
            redraw = 0;
            al_set_target_backbuffer(disp);
            draw_bitmap(buffer, disp);
            draw_palette();
            if(focus == disp)
            {
                draw_crosshair(mx, my, al_map_rgba(255,0,0,150), 2);
                al_draw_textf(default_font, al_map_rgb(255, 0, 255), 0,0, ALLEGRO_ALIGN_LEFT, "(%g, %g) | (%g, %g)", port_x, port_y, xtoa(t, mx), 1-ytob(t, ysize-my-1));
            }
            if(lock_port) draw_crosshair(atox(t, port_x), ysize-1-btoy(t,port_y), al_map_rgba(0,255,0,200), 1);
            //al_draw_filled_circle(atox(t, port_x), ysize-1-btoy(t,port_y), 5, al_map_rgba(255, 0, 0, 100));
    
            flip_display();
        }

        if(draw_portrait && (lock_port || focus == disp))
        {
            draw_portrait = 0;
            port_time = al_current_time();
            printf("%g, %g\n", port_x, port_y);
            draw_port(port_x, port_y, lock_port ? sample_points_lock : sample_points
                      , lock_port ? iter_lock : iter
                      , port_bmp);
            al_set_target_backbuffer(disp2);
            al_clear_to_color(al_map_rgb(0,0,0));
            al_draw_scaled_bitmap(port_bmp, 0,0, port_size, port_size, 0, 0, port_size*port_scale, port_size*port_scale, ALLEGRO_FLIP_VERTICAL);
            flip_display();
        }

    }
    return 0;
}

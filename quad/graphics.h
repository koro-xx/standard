
#ifndef graphics_h
#define graphics_h

#include <signal.h>
#include "calc.h"
#include "mini_graphics.h"

int save_bitmap(dtable *t, const char* filename);
void save_period_graph(dtable *t, const char *filename);
void save_period_graph2(dtable *t, const char *filename);

// temp
void save_period_graph3(dtable *t, const char *filename, float shift);

void save_max_graph(dtable *t, const char *filename);
int save_bitmap_grad(dtable *t, const char* filename, const char *palfile, float tol);
extern volatile sig_atomic_t save_and_exit;

// todo:
// give an argument to save_bitmap(). Use it to pick how to draw it (including the options from save_graph.... It can be a parameter -g <string> and use that string to choose

#endif /* graphics_h */

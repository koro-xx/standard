/*
 * Minimal graphics library (not for display)
 *
 * Copyright (c) 2017 koro
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program (see COPYING.txt and COPYING.LESSER.txt).
 * If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef mini_graphics_h
#define mini_graphics_h

#include <stdint.h>

/* if we have libpng */
#define _LIBPNG 1

/* set to 0 for RGB only (24-bit) */
#define PIXEL_FORMAT_RGBA 1

#if !_LIBPNG
    #undef PIXEL_FORMAT_RGBA
    #define PIXEL_FORMAT_RGBA 0
#endif

typedef struct pixel{
	uint8_t r;
	uint8_t g;
	uint8_t b;
#if PIXEL_FORMAT_RGBA
    uint8_t a;
#endif
} pixel;

typedef struct Canvas{
	int w;
	int h;
	pixel* p;
} Canvas;

/* Color creation macros */
#if PIXEL_FORMAT_RGBA
    #define COLOR_RGBA(r,g,b,a) ((pixel){(r), (g), (b), (a)})
    #define COLOR_RGBA_F(r,g,b,a) ((pixel){((r)*255), ((g)*255), ((b)*255), ((a)*255)})
    #define COLOR_RGB(r,g,b) COLOR_RGBA(r,g,b,255)
    #define COLOR_RGB_F(r,g,b) COLOR_RGBA_F(r,g,b,1.0)
#else
    #define COLOR_RGB(r,g,b) ((pixel){(r), (g), (b))
    #define COLOR_RGB_F(r,g,b) ((pixel){((r)*255), ((g)*255), ((b)*255))
#endif

/* Some default colors */
#define COLOR_NULL (COLOR_RGBA(0,0,0,0))
#define COLOR_BLACK (COLOR_RGB(0,0,0))
#define COLOR_GREY (COLOR_RGB(150,150,150))
#define COLOR_WHITE (COLOR_RGB(255,255,255))
#define COLOR_RED (COLOR_RGB(255,0,0))
#define COLOR_GREEN (COLOR_RGB(0,255,0))
#define COLOR_BLUE (COLOR_RGB(0,0,255))

Canvas *create_canvas(int w, int h);
void destroy_canvas(Canvas *canvas);
void clear_canvas(Canvas *canvas, pixel p);
void set_pixel(Canvas *canvas, int x, int y, pixel p);
int save_canvas_raw(Canvas *canvas, const char* filename);
int save_canvas(Canvas *canvas, const char* filename);

#endif /* def mini_graphics_h */

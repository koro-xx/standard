#include "calc.h"
#include <math.h>
#include <png.h>
#include <stdlib.h>
#include "macros.h"
#include "graphics.h"

/* palette stuff */

pixel *load_palette(const char *filename, int palsize);

pixel pixel_combine(pixel p1, pixel p2, float t)
{
    pixel r;
    
    if((t<0) || (t>1))
        t=t-floor(t);
    
    r.b=(1-t)*p1.b+t*p2.b;
    r.g=(1-t)*p1.g+t*p2.g;
    r.r=(1-t)*p1.r+t*p2.r;
    r.a=255;
    return r;
}


pixel get_color(dtable *t, int n)
{
    if(t->ok[n])
    {
        return (pixel){255, 0, 0, 255};
    }
    else
        return (pixel){0,0,0,0};
}

void put_pixel(Canvas *cv, int x, int y, pixel color)
{
    set_pixel(cv, x, cv->h - y - 1, color);
}

void draw_table_cv(Canvas *cv, dtable *t)
{
    int x,y;
    
    for(y=0; y<t->ysize; y++)
        for(x=0; x<t->xsize; x++)
            put_pixel(cv, x, y, get_color(t, dti_i(t,x,y)));
    
}

/* create period graph */
// todo (new functions)
void draw_period_graph(Canvas *cv, dtable *t, pixel color)
{
    int x, y, xsize = t->xsize, ysize = t->ysize;
    int *g = calloc(xsize, sizeof(*g));
    
    // make graph
    for(x=0; x<xsize; x++)
    {
        for(y=0; y<ysize; y++)
        {
            if(y>x) continue;
            if(!has_interior_i(t,x,y))
            {
                g[x] = y;
            }
        }
    }
    
    int delta = atox(t, 1);

    /* plot ratios */
    for(x=1; x + delta < xsize; x++)
    {
        // remember this is flipped vertically!
        put_pixel(cv, x, ysize*g[x+delta]/g[x], color);
    }
    
    free(g);
}

void draw_max_graph(Canvas *cv, dtable *t, pixel color)
{
    int x, y, xsize = t->xsize, ysize = t->ysize;
    int *g = calloc(xsize, sizeof(*g));
    
    // make graph
    for(x=0; x<xsize; x++)
    {
        for(y=0; y<ysize; y++)
        {
            if(y>x) continue;
            if(!has_interior_i(t,x,y))
            {
                g[x] = y;
            }
        }
    }
    
    
    for(x=1; x < xsize; x++)
    {
        put_pixel(cv, x, g[x], color);
    }
    
    free(g);
}


void save_max_graph(dtable *t, const char *filename)
{
    Canvas *cv = create_canvas(t->xsize, t->ysize);
    clear_canvas(cv, COLOR_RGBA(0,0,0,0));
    draw_max_graph(cv, t, COLOR_BLUE);
    save_canvas(cv, filename);
    destroy_canvas(cv);
}

void save_period_graph(dtable *t, const char *filename)
{
    Canvas *cv = create_canvas(t->xsize, t->ysize);
    clear_canvas(cv, COLOR_RGBA(0,0,0,0));
    draw_period_graph(cv, t, COLOR_BLUE);
    save_canvas(cv, filename);
    destroy_canvas(cv);
}

void save_period_graph2(dtable *t, const char *filename)
{
    Canvas *cv = create_canvas(t->xsize, t->ysize);
    clear_canvas(cv, COLOR_RGBA(0,0,0,0));
    draw_table_cv(cv, t);
    draw_period_graph(cv, t, COLOR_BLUE);
    save_canvas(cv, filename);
    destroy_canvas(cv);
}

// xxx temp
void save_period_graph3(dtable *t, const char *filename, float shift)
{
    Canvas *cv = create_canvas(t->xsize, t->ysize);
    clear_canvas(cv, COLOR_RGBA(0,0,0,255));
    
    int x,y;
    
    for(x=0; x<t->xsize; x++)
        for(y=0; y<t->ysize && y<=x; y++)
            put_pixel(cv, x, y, t->ok[dti_i(t,x,y)] ? COLOR_BLACK : COLOR_RED);
    
    int xsize = t->xsize, ysize = t->ysize;
    int *g = calloc(xsize, sizeof(*g));
    
    // make graph
    for(x=0; x<xsize; x++)
    {
        for(y=0; y<ysize; y++)
        {
            if(y>=x || has_interior_i(t,x,y))
            {
                g[x] = y;
                break;
            }
        }
    }
    
    int delta = atox(t, shift);
    
    /* plot ratios */
    for(x=1; x + delta < xsize; x++)
    {
        put_pixel(cv, x, ysize*g[x+delta]/g[x], COLOR_WHITE);
        put_pixel(cv, x+1, ysize*g[x+delta]/g[x], COLOR_WHITE);
        put_pixel(cv, x, 1+ysize*g[x+delta]/g[x], COLOR_WHITE);
        put_pixel(cv, x-1, ysize*g[x+delta]/g[x], COLOR_WHITE);
        put_pixel(cv, x, -1+ysize*g[x+delta]/g[x], COLOR_WHITE);
    }
    
    free(g);
    save_canvas(cv, filename);
    destroy_canvas(cv);
}

int save_bitmap_grad(dtable *t, const char* filename, const char *palfile, float tol)
{
    Canvas *cv = create_canvas(t->xsize, t->ysize);
    if(!cv) goto Error;
    pixel color;
    pixel *palette = NULL;
    if(palfile)
    {
        load_palette(palfile, 256);
        if(palette) printf("Lodaded palette.\n");
    }
    
    if(!palette) color = COLOR_RED;

    int x,y;
    float c;
    
    for(y=0; y<t->ysize; y++)
        for(x=0; x<t->xsize; x++)
        {
            if(t->ok[dti_i(t,x,y)] || y > x)
                put_pixel(cv, x, y, COLOR_NULL);
            else
            {
                if(palfile){
                    c = min(1, tol*t->mb[dti_i(t,x,y)]);
                    color = palette[(int)(c * 255)];
                }
                put_pixel(cv, x, y, color);
            }
        }
    
    if(!save_canvas(cv, filename)) goto Error;
    destroy_canvas(cv);
    free(palette);
    return 1;
    
Error:
    
    printf("Error saving %s\n", filename);
    if(cv) destroy_canvas(cv);
    
    return 0;
}


int save_bitmap(dtable *t, const char* filename)
{
    Canvas *cv = create_canvas(t->xsize, t->ysize);
    if(!cv) goto Error;
    
    draw_table_cv(cv, t);
    
    if(!save_canvas(cv, filename)) goto Error;
    destroy_canvas(cv);
    return 1;
    
Error:
    
    printf("Error saving %s\n", filename);
    if(cv) destroy_canvas(cv);
    
    return 0;
}




/***********************************************************
 *  Unused functions
 ***********************************************************/
#define MAX_PALSIZE (256*256)
#define MAX_LINE_SIZE 128

pixel *load_palette(const char *filename, int palsize){
    char sline[MAX_LINE_SIZE];
    pixel pal_temp[MAX_PALSIZE];
    int i=0;
    FILE *pal;
    int r, g, b;
    int n = palsize;
    pixel *paleta = NULL;
    
    pal = fopen(filename,"r");
    if(!pal) goto Error;
    
    paleta = malloc(palsize*sizeof(*paleta));
    while( (i<MAX_PALSIZE) && (fgets(sline, MAX_LINE_SIZE-1, pal)) )
    {
        if(sscanf(sline,"%d %d %d",&r,&g,&b)==3)
            pal_temp[i].a =255; pal_temp[i].b=b; pal_temp[i].g = g; pal_temp[i].r=r; i++;
    }
    fclose(pal);
    palsize=i;
    if(palsize < 2) goto Error;

    float t=((float)palsize)/n;
    for(i=0;i<n;i++)
    {
        if(t*i +1 <=palsize)
            paleta[i] = pixel_combine(pal_temp[(int)(t*i)], pal_temp[(int)(t*i) + 1], (t*i - floor(t*i)));
        else
            paleta[i]=pal_temp[(int) t*i];
    }
    /* make sure 0 is black / transparent */
    paleta[0] = (pixel){0,0,0,0};
    return paleta;
    
Error:
    if(paleta) free(paleta);
    return NULL;
    
}


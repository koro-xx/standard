/*
 * Minimal graphics library (not for display)
 *
 * Copyright (c) 2017 koro
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program (see COPYING.txt and COPYING.LESSER.txt).
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mini_graphics.h"
#if !_LIBPNG
    #include "TinyPngOut.h"
#endif

/* have libpng? */
#if _LIBPNG
	#include <png.h>
#endif

#define min(x,y) ((x)<(y) ? x : y)

/* Like memset(), but allows <pat> to be a block of <size> bytes */
void memset_pattern(void *p, void *pat, int size, int n)
{
	int m=0, s=1;
	char *pp = p;

	memcpy(pp, pat, s*size);
	while(s < n)
	{
		memcpy(pp+s*size, p, min(s, n-s)*size);
		s*=2;
	}
}

Canvas *create_canvas(int w, int h)
{
	Canvas *canvas = malloc(sizeof(*canvas));
	if(!canvas) return NULL;
	canvas->p = malloc(w*h*sizeof(*canvas->p));
	if(!canvas->p)
	{
		free(canvas);
		return NULL;
	}
	canvas->w = w;
	canvas->h = h;
	return canvas;
}

void destroy_canvas(Canvas *canvas)
{
	free(canvas->p);
	free(canvas);
}

void clear_canvas(Canvas *canvas, pixel p)
{
	memset_pattern(canvas->p, &p, (sizeof *canvas->p), canvas->w*canvas->h);
}

void set_pixel(Canvas *canvas, int x, int y, pixel p)
{
	// silently ignore pixels outside the canvas
	if(x<0 || y<0 || x >= canvas->w || y >= canvas->h)
		return;

	canvas->p[(y * canvas->w) + x] = p;
}

#if !_LIBPNG
int save_canvas(Canvas *canvas, const char* filename)
{
	FILE *fout = fopen(filename, "wb");
	struct TinyPngOut pngout;

	if (fout == NULL ||
			TinyPngOut_init(&pngout, fout, canvas->w, canvas->h) != TINYPNGOUT_OK)
		goto error;

	if (TinyPngOut_write(&pngout, (uint8_t*) canvas->p, canvas->w*canvas->h)
		!= TINYPNGOUT_OK)
		goto error;

	if (TinyPngOut_write(&pngout, NULL, 0) != TINYPNGOUT_DONE)
		goto error;

	fclose(fout);
	return 1;

error:
	fprintf(stderr, "Error writing %s\n", filename);
	if (fout != NULL)
		fclose(fout);
	return 0;
}
#endif

#if _LIBPNG
/* save_png:
 *  Core save routine for png images.
 */
static int save_png(png_structp png_ptr, Canvas *canvas)
{
   const int bmp_h = canvas->h;
   int y;

   for (y = 0; y < bmp_h; y++) {
      uint8_t *p = ((uint8_t *)canvas->p) + sizeof(pixel)*canvas->w*y;
      png_write_row(png_ptr, p);
   }

   return 1;
}

int save_canvas(Canvas *canvas, const char* filename)
{
   jmp_buf jmpbuf;
   png_structp png_ptr = NULL;
   png_infop info_ptr = NULL;
   int colour_type;
   FILE *fp = fopen(filename, "wb");

	 if (fp == NULL) goto Error;

   /* Create and initialize the png_struct with the
    * desired error handler functions.                */
   png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
                                     (void *)NULL, NULL, NULL);
   if (!png_ptr) goto Error;

   /* Allocate/initialize the image information data. */
   info_ptr = png_create_info_struct(png_ptr);
   if (!info_ptr) goto Error;

	 png_init_io(png_ptr, fp);

   /* Set error handling.
	 	* If a libpng function fails, execution jumps here.
	 	*/
	 if (setjmp(jmpbuf)) goto Error;
	 //png_set_error_fn(png_ptr, jmpbuf, user_error_fn, NULL);

   /* Set the image information here.  Width and height are up to 2^31,
    * bit_depth is one of 1, 2, 4, 8, or 16, but valid values also depend on
    * the color_type selected. color_type is one of PNG_COLOR_TYPE_GRAY,
    * PNG_COLOR_TYPE_GRAY_ALPHA, PNG_COLOR_TYPE_PALETTE, PNG_COLOR_TYPE_RGB,
    * or PNG_COLOR_TYPE_RGB_ALPHA.  interlace is either PNG_INTERLACE_NONE or
    * PNG_INTERLACE_ADAM7, and the compression_type and filter_type MUST
    * currently be PNG_COMPRESSION_TYPE_BASE and PNG_FILTER_TYPE_BASE.
    */
#if PIXEL_FORMAT_RGBA
    colour_type = PNG_COLOR_TYPE_RGB_ALPHA;
#else
    colour_type = PNG_COLOR_TYPE_RGB
#endif

   /* Set compression level. */
   png_set_compression_level(png_ptr, 3);

   png_set_IHDR(png_ptr, info_ptr,
                canvas->w, canvas->h,
                8, colour_type,
                PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE,
                PNG_FILTER_TYPE_BASE);

   /* Optionally write comments into the image ... Nah. */

   /* Write the file header information. */
   png_write_info(png_ptr, info_ptr);

   /* Once we write out the header, the compression type on the text
    * chunks gets changed to PNG_TEXT_COMPRESSION_NONE_WR or
    * PNG_TEXT_COMPRESSION_zTXt_WR, so it doesn't get written out again
    * at the end.
    */
   if (!save_png(png_ptr, canvas)) goto Error;

   png_write_end(png_ptr, info_ptr);

   png_destroy_write_struct(&png_ptr, &info_ptr);

	 fclose(fp);
   return 1;

 Error:

	printf("Error in save_canvas_png()\n");
   if (png_ptr) {
      if (info_ptr)
         png_destroy_write_struct(&png_ptr, &info_ptr);
      else
         png_destroy_write_struct(&png_ptr, NULL);
   }
	 if(fp) fclose(fp);

   return 0;
}
#endif //libpng

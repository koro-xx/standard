#ifndef calc_h
#define calc_h

#include <signal.h>


/* Our displacement function */
#define PHI(t) (sin(2.0*pi*(t)))
#define MIN_DISP 1.0

// get index corresponding to (a,b) in dtable and other transformations
#define atox(t, a) ( ((int)((t)->xsize*((a) - (t)->a0)/((t)->a1 - (t)->a0))) )
#define btoy(t, b) ( ((int)((t)->ysize*((b) - (t)->b0)/((t)->b1 - (t)->b0))) )
#define xtoa(t, x) ( (((x)*((t)->a1 - (t)->a0))/(t)->xsize + (t)->a0) )
#define ytob(t, y) ( (((y)*((t)->b1 - (t)->b0))/(t)->ysize + (t)->b0) )

#define dti_i(t, x, y) ( ((t)->xsize*(y) + x) )
#define dti(t, a, b) ( dti_i(t, atox(t, a), btoy(t, b)) )
#define itox(t, n) ( ((n) % (t)->xsize) )
#define itoy(t, n) ( ((n) / (t)->xsize) )

#define has_interior(t, a, b) ( ((t)->ok[dti(t,a,b)]) )
#define has_interior_i(t, x, y) ( ((t)->ok[dti_i(t,x,y)]) )

typedef struct int2
{
    int x;
    int y;
} int2;

typedef struct pt_list
{
    int2 val;
    struct pt_list *next;
} pt_list;

// todo: store maximizing point in dtable + level
typedef struct dtable
{
    /* pixel count */
    int xsize, ysize;
    /* auxiliary */
    int levels;
    /* real dimensions */
    double a0, b0, a1, b1;
    /* 0 if we can't say it's interior, otherwise iter min to be in interior */
    int *ok;
    /* maximizing parameter */
    double *ma;
    double *mb;
    /* maximizing point if ok=1 [maxdisp if ok = 0 (todo)]*/
    double *mx;
    double *my;
} dtable;

int pt_list_len(pt_list *head);
void pt_list_destroy(pt_list **head);


struct dtable *create_dtable(void);
void destroy_dtable(dtable *t);
/* allocs (zeroed) memory for arrays in table, and set .xsize, .ysize.
 * does not change other values */
int init_dtable (dtable *t, int xsize, int ysize);
int save_dtable(dtable *dt, const char *filename);
struct dtable *load_dtable(const char *filename);
void dtable_print_params(dtable *t);
dtable *join_dtable(dtable *t0, dtable *t1);

/* does a full pass, 1 point per pixel, 1 orbit per point, full range */
void full_pass(dtable *t, int iter);

/* similar to full but only goes through points not yet in the interior */
void second_pass(dtable *t, int iter);

/* finds maximum displacement, both horizontal and veritcal. If it is greater
 *than MIN_DISP in both directions, store coords/parameters and set is_ok
 */
int find_max_disp(dtable *t, double a, double b, double x, double y, int iter);

/* goes through points in the boundary only */
pt_list *bd_pass(dtable *t, pt_list **bd, int iter);

/* make list of boundary points (exterior) */
pt_list *find_bd(dtable *t);


/* Returns 1 if pixel (x,y) is in the boundary (from the outside) */
int is_bd(dtable *t, int x, int y);

/* If some neighbor of (x,y) is in the boundary, add it to the list */
void update_bd(dtable *t, pt_list **bd, int x, int y);

extern volatile sig_atomic_t save_and_exit;
extern int use_shearless;

#endif /* def calc_h */

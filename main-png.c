/*  libpng version. does not require allegro.

gcc -Ofast -I/usr/local/include -L/usr/local/lib -lpng ./main-png.c calc.c graphics.c mini_graphics.c [TinyPNGOut.c]

gcc -Ofast -I/usr/local/include -L/usr/local/lib -lpng  main-png.c calc.c

TODO:
    -use MAX_OPT preprocessor var to use only y coordinate and other optimizations
    -improve choice of iter and min_new_points (optimize?)
    -use join_table and create_period_graph (calc.c and graphics.c)
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <time.h>
#include "graphics.h"
#include "macros.h"
#include "calc.h"

#define BD_COLOR ( ((pixel){255,255,255,255}) )

volatile sig_atomic_t save_and_exit = 0;

int save_bitmap(dtable *t, const char *filename);


int join_table_f(const char *file1, const char *file2, const char *outfile)
{
    dtable *t1 = load_dtable(file1);
    dtable_print_params(t1);
    
    dtable *t2 = load_dtable(file2);
    dtable_print_params(t2);

    dtable *t = join_dtable(t1, t2);
    if(!t) goto Error;

    save_dtable(t, outfile);
    printf("Result:\n");
    dtable_print_params(t);

    destroy_dtable(t1);
    destroy_dtable(t2);
    
    return 1;
    
Error:
    return 0;
}

void sig_handler(int signo)
{
    if (signo == SIGINT)
    {
        printf("\n*** Received SIGINT. Exiting after this round *** \n\n");
        save_and_exit=1;
    }
}

int main(int argc, char **argv)
{
    dtable *t=NULL;
    float ratio = 1;
    int xsize = 500, ysize = 500;
    int i=0, opt = 0, iter = 0;
    
    /* parameters */
    double a0 = 0, b0 = 0, a1 = 1, b1 = 1;

    int pix_max = 500000,
        iter_start = 1000,
        iter_max = 1000000,
        min_new_points = 30,
        reps_per_level = 10,
        bd_pass_factor = 10,
        reps1 = 0, /* number of full passes */
        reps2 = 4,
        reps3 = 10; /* number of reps per level in boundary pass */

    
    int load_flag =0, save_flag =1, opt_1 = 0, opt_2 = 0, opt_r = 0, opt_F=0, opt_x=0, opt_p=0, opt_j=0, opt_g=0;
    char *dtable_file = "standard.tbl";
    char *image_file = "standard.png";
    char *palfile = NULL;
    /* seconds */
    float   save_image_dt = 300,
            save_dtable_dt = 1800;

    /* attempt to catch SIGINT */
    if (signal(SIGINT, sig_handler) == SIG_ERR)
        printf("\ncan't catch SIGINT\n");


    /* Parse args and initialize data */

    i=0;
    while ((opt = getopt(argc, argv, "l1:2:3Fjc:g:s:p:x:I:r:k:i:m:b:a:S:L:o:t:")) != -1)
    {
        i++;
        switch(opt)
        {
            case 'l': load_flag = 1; break;
            case 's': save_flag = atoi(optarg); break;
            case 'r': opt_r = atoi(optarg); break;
            case 'p': opt_p = atoi(optarg); pix_max = opt_p; break;
            case 'I': iter_max = atoi(optarg); break;
            case 'k': min_new_points = atoi(optarg); break;
            case 'i': iter = atoi(optarg); break;
            case '1': reps1 = opt_1 = atoi(optarg); break;
            case '2': reps2 = opt_2 = atoi(optarg); break;
            case '3': reps3 = 0; break;
            case 'S': dtable_file = optarg; break;
            case 'L': dtable_file = optarg; load_flag = 1; break;
            case 'm': bd_pass_factor = atoi(optarg); break;
            case 'F': opt_F=1; break;
            case 'x': opt_x = atoi(optarg); break;
            case 'o': image_file = optarg; break;
            case 'j': opt_j = 1; break;
            case 'g': opt_g = atoi(optarg); break;
            case 'c': palfile = optarg; break;
            case 't': use_shearless = atoi(optarg); break;
            default: goto Usage;
        }
    }
    
    if(i==0 && argc <= 1) goto Usage;

    if(opt_r) reps_per_level = opt_r;

   
    if(opt_g)
    {
        float tol=0;
        if(palfile)
        {
            if(optind < argc)
                tol = atof(argv[optind]);
            else
                tol = 2;
        }
        t = load_dtable(dtable_file);
        if(!t) return -1;
        dtable_print_params(t);
        printf("Creating image...\n");
        save_bitmap_grad(t, image_file, palfile, tol, opt_g);
        exit(0);
    }
    
//    if(opt_g)
//    {
//        t = load_dtable(dtable_file);
//        if(!t) return -1;
//        dtable_print_params(t);
////        opt_g = optind < argc ? atoi(argv[optind]) : 0;
//        printf("Creating image...\n");
//        save_bitmap_grad(t, image_file, )
//        if(opt_g == 0)
//            save_bitmap_grad(t, image_file, NULL, 0);
//        else if(opt_g == 1)
//            save_period_graph(t, image_file);
//        else if(opt_g == 2)
//            save_period_graph2(t, image_file);
//        else if(opt_g == 3)
//            save_max_graph(t, image_file);
//        exit(0);
//    }
    
    if(save_flag && !load_flag && !opt_F && (access( dtable_file, F_OK ) != -1) )
    { /* savefile exists */
        printf("Savefile %s exists. Use -F to force overwrite.\n", dtable_file);
        exit(0);
    }

    // todo: try to join tables
    if(opt_j)
    {
        if(argc - optind < 2)
        {
            printf("Need 2 arguments.\n");
            exit(0);
        }
        join_table_f(argv[optind], argv[optind+1], dtable_file);
        exit(0);
    }
    
    if(load_flag) goto Load;
    
    i = optind;
    if(argc - i < 4)
    {
        Usage:
        printf("Usage: <progname> [-options] a0 a1 b0 b1\n"
               "\nOptions:\n"
               "-s [0:1]     Save table periodically? Default = 1\n"
               "-l           Load default savefile and continue\n"
               "-S [file]    Savefile for table. Default = standard.tbl\n"
               "-L [file]    Load [file] and continue.\n"
               "-p [num]     Max number of pixels. Uses best resolution matching aspect ratio\n"
               "-x [num]     Set xsize in pixels. ysize will match aspect ratio\n"
               "-I [num]     Max iterations (stops increasing after that)\n"
               "-r [reps]    Reps per level\n"
               "-k [num]     Minimum number of new pixels per pass (averaged over rep)\n"
               "-i [iter]    Set initial iterations\n"
               "-m [num]     Multiplier for boundary pass\n"
               "-1 [num]     Do [reps] first passes first (default = 0)\n"
               "-2 [reps]    Do [reps] base passes first (default = 4)\n"
               "-3           Do not do boundary passes\n"
               "-o [file]    Output image file (png)\n"
               "-g           Save picture of points with 0 vertical displacement\n"
               "-j [f1] [f2] Join two table files. They must be adjacent regions\n"
               "-c [palfile] Use palette from palfile to draw gradient\n"
               "-t [num]     Sample point to use [0=random, 1=shearless_v, 2=shearless_h]\n"
               );
        exit(0);
    }
    else
    {
        a0 = atof(argv[i++]);
        a1 = atof(argv[i++]);
        b0 = atof(argv[i++]);
        b1 = atof(argv[i++]);
    }

    while (++i < argc) printf ("--- Ignored argument: %s\n", argv[i]);

    /* Finished parsing args */

Load:
    if(load_flag)
    {
        if(!opt_1) reps1 = 0;
        if(!opt_2) reps2 = 0;
        printf("Loading %s\n", dtable_file);
        t = load_dtable(dtable_file);
        if(!t) return -1;
        xsize = t->xsize;
        ysize = t->ysize;
        if(!iter) iter = t->levels; /* if iter is already set, use it */
        dtable_print_params(t);
    }
    else
    {
        ratio = (b1-b0)/(a1-a0);
        if(opt_x)
        {
            xsize = opt_x;
            ysize = ratio*xsize;

            if(opt_p && xsize*ysize > pix_max)
            {
                printf("Dimensions %d x %d exceed pix_max = %d\n",
                        xsize, ysize, pix_max);
                exit(0);
            }
        }
        else
        {
            if(opt_p) pix_max = opt_p;
            xsize = sqrt(pix_max/ratio);
            ysize = ratio*xsize;
        }

        if(xsize<=0 || ysize <= 0)
        {
            printf("Bad dimensions %d x %d. \n", xsize, ysize);
            exit(0);
        }

        t = create_dtable();
        t->a0 = a0;
        t->a1 = a1;
        t->b0 = b0;
        t->b1 = b1;
        init_dtable(t, xsize, ysize);
        dtable_print_params(t);
    }
    if (!iter) /* if iter is already set, don't change it */
        iter = iter_start;
    
    /* If iter_max is smaller than iter, increase it */
    iter_max = max(iter, iter_max);
    
    t->levels = iter;
    
    /* run first full pass
     * note: full pass is outdated / unnecessary. doing second pass directly is enough
     * we leave thefault reps1 = 0 to skip this step.
     */
    
    for(i=0; i < reps1; i++)
    {
        printf("Full pass, rep %d/%d... \n", i+1, reps1);
        full_pass(t, iter);
        if(save_flag) save_dtable(t, dtable_file);
        save_bitmap(t, image_file);
    }
    
    /* run second pass (base) */
    printf("Base pass...\n");
    for(i=0; i < reps2; i++)
    {
        printf("Base pass, iter = %d, rep %d/%d... \n", iter, i+1, reps2);
        second_pass(t, iter);
        if(save_flag) save_dtable(t, dtable_file);
        save_bitmap(t, image_file);
    }
    
    /* After second pass, increase iter */
    if(reps2 > 0)
    {
        iter *= bd_pass_factor;
        t->levels=iter;
    }
    
    if(reps3 == 0) goto End;

    /* Find the boundary */
    printf("Boundary detection... ");
    pt_list *bd_list = find_bd(t);
    pt_list *new_pts = NULL;
    printf("%d boundary points.\n", pt_list_len(bd_list));

    /* Boundary pass */
    float new_t, start_t, save_image_t, save_dtable_t;
    new_t = start_t = save_image_t = save_dtable_t = time_sec();
    printf("Starting boundary pass with iter=%d\n", iter);
    while(1)
    {
        int count = 0;
        for(int j=0; j<reps_per_level; j++)
        {
            int new_pts_n=0;
            /* do a boundary pass */
            new_pts = bd_pass(t, &bd_list, iter);

            /* exit was requested */
            if(save_and_exit) goto Exit;

            new_pts_n = pt_list_len(new_pts);
            count += new_pts_n;
            pt_list_destroy(&new_pts);

            new_t = time_sec()
            printf("%*d min, rep %d/%d, found %d\n", 4, (int)((new_t-start_t)/60), j+1, reps_per_level, new_pts_n);

            if( new_t - save_image_t > save_image_dt )
            {
                save_image_t = time_sec();
                save_bitmap(t, image_file);
            }

            if( save_flag && (new_t - save_dtable_t > save_dtable_dt) )
            {
                save_dtable_t = time_sec();
                printf("Saving %s...\n", dtable_file);
                save_dtable(t, dtable_file);
            }
        }
        printf("iter = %d, bd = %d, found in last set: %d\n", iter, pt_list_len(bd_list), count);

        /* increase level if not finidning enough points */
        // todo: keep track of pps and update accordingly
        if(count < min_new_points*reps_per_level)
        {
            iter = min(iter_max, iter*2);
            t->levels = iter;
            printf("Increasing iter to %d.\n", iter);
        }
    }

    return 0;

    Exit:
    {
        if(save_flag) save_dtable(t, dtable_file);
        save_bitmap(t, image_file);
    End:
        exit(0);
    }
}


